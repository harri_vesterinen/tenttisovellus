import React, { useState, useEffect, useContext } from 'react'; // useReducer poistettu
//import kuvaPepsu from './images/Pepsu_1.jpg';
import SentimentVerySatisfiedSharpIcon from '@material-ui/icons/SentimentVerySatisfiedSharp';
import SentimentVeryDissatisfiedSharpIcon from '@material-ui/icons/SentimentVeryDissatisfiedSharp';
//import testdata from './testdata'; siirretty Context.js:ään
import './App.css';

import { Box, Button, Checkbox, FormLabel } from '@material-ui/core'; // Container poistettu
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/core/styles'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { green, blue } from '@material-ui/core/colors'; // red poistettu
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import AddCircleOutlineRoundedIcon from '@material-ui/icons/AddCircleOutlineRounded';
//import DeleteForeverRoundedIcon from '@material-ui/icons/DeleteForeverRounded';
//import { maxWidth, minWidth } from '@material-ui/system';
import ButtonAppBar from './components/AppBar/ButtonAppBar'

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import LogIn from './components/LogIn/LogIn'
import Stats from './components/Stats/Stats'
import Admin from './components/Admin/Admin'
import DialogAlertExamDelete from './components/Dialogs/DialogAlertExamDelete'
import {TenttiContext} from './Context'
import TenttiSort from './components/TenttiSort/TenttiSort'
import GetUserToken from './components/Functions/GetUserToken'
//import DialogAlertInfoOfApp from './components/Dialogs/DialogAlertInfoOfApp';

const axios = require('axios');

// ********************************

//const alkutila = testdata; // siirretty, ks. Context.js

// **************  KYSYMYS  ***************
const Kysymys = (props) => {

    //const [tentit, dispatch] = useReducer(reducer, alkutila) // reducer-funktio määritetty App.js alussa
    const [kaikkiOikein, setKaikkiOikein] = useState(true)

    //********* */
    const useStyles = makeStyles(theme => ({
        container: {
          display: 'flex',
          flexWrap: 'wrap',
          marginLeft: theme.spacing(2),
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 800,
        },
        formControl: {
            margin: theme.spacing(2),
            marginLeft: theme.spacing(1),
            minWidth: 120,
        },
        
      }));
    const classes = useStyles();
    /************ */

    // vihreä CheckBox
    const GreenCheckbox = withStyles({
        root: {
          color: green[400],
          '&$checked': {
            color: green[600],
          },
        },
        checked: {},
    })(props => <Checkbox color="default" {...props} />);

    // sininen CheckBox
    const BlueCheckbox = withStyles({
        root: {
          color: blue[400],
          '&$checked': {
            color: blue[600],
          },
        },
        checked: {},
    })(props => <Checkbox color="default" {...props} />);

    // **********************************************************
    // **********************************************************
    // kun painettu 'näytä vastaukset'-nappia App-komponentissa:
    useEffect( () => {
        
        setKaikkiOikein(true)
        props.vastaukset.map( vastaus => {
            if (vastaus.kayttajan_vastaus !== vastaus.oikein) { // oli: vastaus.valittu !== vastaus.oikeavastaus
                setKaikkiOikein(false)
            }
            return vastaus
        } )

    }, [props.naytaVastaukset] )

    // Kysymys-piirto
    return (
    
        <Paper>
            <Box // key={props.tenttiIndex + props.kysymysIndex}>
            >
                <p><br /></p>
                
                { /* EI KÄYTÖSSÄ NYT, tiedot ovat alemman tekstikentän labelissä
                <TextField //key={props.tenttiIndex + props.kysymysIndex + props.aihepiiri_id} //????????????
                    required
                    id={props.tenttiIndex + props.kysymysIndex + props.aihepiiri_id} 
                    //label={props.tenttiNimi}
                    //defaultValue={props.kysymys}
                    //value={ (props.kysymysIndex + 1) + ". " + (props.kysymys) }
                    //value={"Aihepiiri: " + props.aihepiiri_nimi}
                    className={classes.textField}
                    //helperText={props.tenttiNimi}
                    margin="none"
                    //variant="filled"
                    variant="standard"             
                />
                */}

                <TextField
                    required
                    id={props.tenttiIndex + props.kysymysIndex + props.teksti}
                    label={"Aihepiiri: " + props.aihepiiri_nimi + ", " + props.tenttiNimi}
                    //defaultValue={props.kysymys}
                    //value={ (props.kysymysIndex + 1) + ". " + (props.kysymys) }
                    value={ (props.kysymysIndex + 1) + ". " + (props.teksti) }
                    className={classes.textField}
                    //helperText={props.tenttiNimi}
                    margin="none"
                    variant="outlined"
                    //variant="normal"           
                />

                {props.naytaVastaukset === true && kaikkiOikein === true &&
                    <SentimentVerySatisfiedSharpIcon color="primary" fontSize="large" titleAccess="Oikein!"></SentimentVerySatisfiedSharpIcon>
                    //<img src={kuvaPepsu} width="55" height="55" alt="Oikein!" />}
                }

                {props.naytaVastaukset === true && kaikkiOikein === false &&
                    <SentimentVeryDissatisfiedSharpIcon color="error" fontSize="large" titleAccess="Väärin!"></SentimentVeryDissatisfiedSharpIcon>
                }
                                       
            </Box>
                       
            {props.vastaukset.map( (vastaus, i) => (                            
                <Box key={i.toString()}>
                    <FormLabel>                       
                        <GreenCheckbox 
                            checked={vastaus.kayttajan_vastaus} 
                            onChange={props.naytaVastaukset === false ? 
                                // 1) hae GET:llä, jos löytyy->PUT, ei löydy->POST
                                (event, currentValue) => {
                                    currentValue = event.target.checked
                                    // onko käyttäjän vastaus jo olemassa?
                                    // SQL: // SQL: GET / localhost:5000/opiskelijaVastaus/{kayttaja_id}&{vastaus_id}
                                    var userToken = GetUserToken()
                                    axios.get(props.url + 'opiskelijaVastaus/' + props.kayttaja_id + '&' + vastaus.id, { headers: {"x-access-token" : `${userToken}`} } )
                                        .then(function (responseGet) {
                                            if (responseGet.data.length === 0) { // ei löydy dataa, luodaan vastaus käyttäjälle
                                                console.log('Vastaus: ' + vastaus.id + ', ei löydy vielä käyttäjälle: ' + props.kayttaja_id + '. Luodaan vastaus arvolla: ' + currentValue)
                                                // SQL: POST / localhost:5000/opiskelijaVastaus/{kayttaja_id}&{vastaus_id}, arvo bodyssä
                                                axios.post(props.url + 'opiskelijaVastaus/' + props.kayttaja_id + '&' + vastaus.id, {
                                                    kayttajan_vastaus: currentValue
                                                } , { headers: {"x-access-token" : `${userToken}`} } )
                                                    .then(function (responsePost) {
                                                        console.log('Oletusvastaus: ' + vastaus.id + ' luotiin käyttäjälle: ' + props.kayttaja_id)
                                                        console.log(responsePost)
                                                    })
                                                    .catch(function (error) {
                                                        console.error('Oletusvastauksen lisääminen epäonnistui (' + error + ')');
                                                        alert('Oletusvastauksen lisääminen epäonnistui. Kirjaudu sisään uudelleen sovellukseen, mikäli virhe toistuu.')
                                                        //handleKirjauduUlos()
                                                    });
                                            } else { // dataa löytyy, ei luoda uutta vastausta käyttäjälle
                                                console.log('Vastaus: ' + vastaus.id + ' löytyi käyttäjälle: ' + props.kayttaja_id + ', ei luoda uutta vastausta.')
                                                console.log(responseGet) }
                                        })
                                        .catch(function (error) {
                                            console.error('Opiskelijan vastauksen haku epäonnistui (' + error + ')');
                                            //alert('Opiskelijan vastauksen haku epäonnistui. Kirjaudu sisään uudelleen sovellukseen, mikäli virhe toistuu.')
                                            //handleKirjauduUlos()
                                        });

                                    //currentValue = event.target.checked
                                    // päivitetään käyttäjän vastaus
                                    // SQL-KYSELY: PUT / localhost:5000/opiskelijaVastaus/{kayttaja_id}&{vastaus_id}, vastaus Bodyssä
                                    console.log('PUT: ' + props.url + 'opiskelijaVastaus/' + props.kayttaja_id + '&' + vastaus.id)
                                    axios.put(props.url + 'opiskelijaVastaus/' + props.kayttaja_id + '&' + vastaus.id, { 
                                        kayttajan_vastaus: currentValue
                                        } , { headers: {"x-access-token" : `${userToken}`} } )
                                        .then(function (response) {
                                            console.log(response);
                                            //console.log('vastaus_id: ' + response.data[0].id);
                                            props.dispatch(
                                                {   nimi: "checkboxChangeUser",
                                                    arvo: currentValue,
                                                    tapahtumatyyppi: 'muuta',
                                                    tenttiIndex: props.tenttiIndex,
                                                    kysymysIndex: props.kysymysIndex,
                                                    vastausIndex: i,
                                                    //id: response.data[0].id
                                                }
                                            )
                                        })
                                        .catch(function (error) {
                                            console.error('Vastauksen päivittäminen epäonnistui (' + error + ')');
                                            alert('Vastauksen päivittäminen epäonnistui. Kirjaudu sisään uudelleen sovellukseen, mikäli virhe toistuu.')
                                            //handleKirjauduUlos()
                                        });
        
                                    // VANHA:
                                    /*
                                    
                                        props.dispatch(
                                            {nimi: "checkboxChangeUser",
                                            arvo: event.target.checked,
                                            tapahtumatyyppi: 'muuta',
                                            tenttiIndex: props.tenttiIndex,
                                            kysymysIndex: props.kysymysIndex,
                                            vastausIndex: i }
                                        ) : undefined }

                                    */

                            } // onChange loppu
                            : undefined }                                      
                        >
                        </GreenCheckbox>
                    </FormLabel>
                    {props.naytaVastaukset === true &&
                    <FormControlLabel
                            control={
                                <BlueCheckbox checked={vastaus.oikein} /> // oli ennen: vastaus.oikeavastaus
                             }
                            label=""
                    />
                    }
                    {vastaus.teksti}
                </Box>
                ))
            }

        </Paper>
    );
}

//************  TENTTIAPP  ************************** */
const TenttiApp = () => {

    const { tentit, dispatch } = useContext(TenttiContext);
    //VANHA: const [tentit, setTentit] = useState(testdata) // siirretty Context.js: const alkutila = testdata;
    const [kayttaja, setKayttaja] = useState(null)
    const [onkoAdmin, setOnkoAdmin] = useState(false)
    const [kayttajaNimi, setKayttajaNimi] = useState(null)
    const [valittuTentti, setValittuTentti] = useState(null)
    const [naytaVastaukset, setNaytaVastaukset] = useState(false)
    const [komponenttiAlustettu, setKomponenttiAlustettu] = useState(false)
    const [aakkosjärjestys, setAakkosjärjestys] = useState(true)

    // 'express-palvelin'-hakemiston palvelin: portissa 5000
    const [url, setUrl] = useState("http://localhost:5000/");
    // Admin: kysymysten kaikki aihealueet
    const [aihePiirit, setAihePiirit] = useState(undefined)
    // Admin: tentin kaikki admin-tilat
    const [tentinTilat, setTentinTilat] = useState(undefined)
    // Admin: tentin kaikki arvosteluasteikot
    const [tentinArvosteluAsteikot, setTentinArvosteluAsteikot] = useState(undefined)
    // onko 'Tentit'-hakua painettu
    const [onkoTentitHaettu, setOnkoTentitHaettu] = useState(false)

    //********* */
    const useStyles = makeStyles(theme => ({
        container: {
          display: 'flex',
          flexWrap: 'wrap',
          marginLeft: theme.spacing(2),
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 700,
        },
        formControl: {
            margin: theme.spacing(2),
            marginLeft: theme.spacing(1),
            minWidth: 120,
        },
        
      }));
    const classes = useStyles();
    /************ */

    // Kirjaudu sisään UUSI
    const handleKirjauduSisaanUUSI = (kayttajatunnus, salasana) => {
        console.log('Kirjaudu sisään kayttajatunnus: ' + kayttajatunnus)
        // localhost:5000/api/auth/login
        axios.post(url + 'api/auth/login', { 
            nimi: kayttajatunnus,
            salasana: salasana
          })
          .then(function (response) {
            console.log(response);
            // tokenin talletus localStorageen
            localStorage.setItem('TenttiAppUserToken', JSON.stringify(response))
            var userToken = JSON.parse(localStorage.getItem('TenttiAppUserToken')).data.token
            //var userToken = response.data[0].token

            // käyttäjätietojen haku tietokannasta
            // localhost:5000/api/auth/me
            // headerissä: 'x-access-token', jonka arvona tokenin arvo
            getUserData(userToken)
            //alert('getUserDate-funktio suoritettu ok')
          })
          .catch(function (error) {
              if (error.response.status===404) { // käyttäjää ei löydy
                console.log('Käyttäjää ei löydy, ' + error.response.data); // NÄYTÄ VIRHE KÄYTTÄJÄLLE
                alert('Kirjautuminen epäonnistui. Yritä uudelleen. (Virhe: käyttäjää ei löydy,  ' + error.response.data + ').')
              } else if (error.response.status===401) { // salasana väärin
                console.log('Salasana väärin, ' + error.response.data); // NÄYTÄ VIRHE KÄYTTÄJÄLLE
                alert('Kirjautuminen epäonnistui. Yritä uudelleen. (Virhe: salasana väärin).')
              } else {
                console.log(error); // NÄYTÄ VIRHE KÄYTTÄJÄLLE
                alert('Kirjautuminen epäonnistui. Yritä uudelleen. (Virhe: ' + error.response.data + ').')
              }
          });
    }

    // käyttäjätietojen haku tietokannasta tokenin avulla
    // localhost:5000/api/auth/me
    const getUserData = (userToken) => {
        console.log('getUserData-funktio')
        //if (!userToken) return null
        // headerissä: 'x-access-token', jonka arvona tokenin arvo
        console.log( url + 'api/auth/me , { headers: {"x-access-token" : `${'+userToken+'}`} }')
        //EI TOIMI: axios.get(url + 'api/auth/me' , { headers: {"Authorization" : `${userToken}`} })
        axios.get(url + 'api/auth/me' , { headers: {"x-access-token" : `${userToken}`} })
            .then(resUser => {
                console.log('Käyttäjä id: ' + resUser.data[0].id + ', nimi: ' + resUser.data[0].nimi + ', käyttäjätyyppi: '+ resUser.data[0].kayttajatyyppi_id);
                //alert(resUser.data)
                setKayttaja( resUser.data[0].id)
                setKayttajaNimi( resUser.data[0].nimi)
                if (resUser.data[0].kayttajatyyppi_id === '1') {
                    console.log('Admin-käyttäjä.')
                    setOnkoAdmin(true)
                } else {
                    console.log('Opiskelijakäyttäjä.')
                    setOnkoAdmin(false)
                }
            })
            .catch((error) => {
                console.log(error)
                alert('Virhe käyttäjän tietojen hakemisessa (Virhe: ' + error + ')')
            });
    }

    // Kirjaudu sisään VANHA ilman token tunnistautumista
    const handleKirjauduSisaan = (kayttajatunnus, salasana, onkoAdmin) => {       
        if(kayttajatunnus !== "" && salasana !== "") {
            setKayttaja( kayttajatunnus )
            if (onkoAdmin) {
                setOnkoAdmin(true)
            } else {
                setOnkoAdmin(false)
            }
        } else {
            setKayttaja(null)
            setOnkoAdmin(false)
        }
        console.log('kayttajatunnus:' + kayttajatunnus + ', onko Admin: ' + onkoAdmin)  
    }

    // Kirjaudu ulos
    const handleKirjauduUlos = (kayttajatunnus) => {
        console.log('kirjaudu ulos kayttajatunnus:' + kayttajatunnus) 
        setValittuTentti(null)
        setNaytaVastaukset(false)    
        setKayttaja(null)
        setOnkoAdmin(false)
        setKayttajaNimi(null)
        // poista Token localStoragesta:
        localStorage.removeItem('TenttiAppUserToken')
        console.log('Käyttäjän Token poistettu localStoragesta ok.')
        localStorage.removeItem('tenttiappTentit')
        console.log('Käyttäjän Tentit poistettu localStoragesta ok.')
    }

    // Rekisteröidy (opiskelija)
    const handleOpiskelijaRekisteroidy = (kayttajatunnus, salasana, onkoAdmin) => {
        console.log('Rekisteröi opiskelijaksi kayttajatunnus: ' + kayttajatunnus + ", salasana: " + salasana + ", onko Admin: " + onkoAdmin)
        let created = new Date()
        // axios.get(url, { crossdomain: true }) EI TARTTE
        axios.post(url + 'api/auth/register', { // vanha endpoint: 'rekisteroiOpiskelija'
            nimi: kayttajatunnus,
            salasana: salasana,
            kayttajatyyppi_id: '2', // 2 = opiskelija
            created: created
          })
          .then(function (response) {
            console.log(response);
            alert('Rekisteröinti onnistui. Kirjaudu nyt sisään sovellukseen.')
            document.getElementById('username').value = ''
            document.getElementById('password').value = ''
          })
          .catch(function (error) {

            if (error.response.status===500) { // käyttäjänimi löytyy jo, virhe
                console.log('Käyttäjä löytyy jo, ' + error.response.data); // NÄYTÄ VIRHE KÄYTTÄJÄLLE
                alert('Rekisteröinti epäonnistui, koska käyttäjänimi on jo olemassa. Yritä uudelleen. (Virhe:' + error + ')' )
            } else {
                console.log(error); // NÄYTÄ VIRHE KÄYTTÄJÄLLE
                alert('Kirjautuminen epäonnistui. Yritä uudelleen. (Virhe: ' + error + ').')
            }
            document.getElementById('username').value = ''
            document.getElementById('password').value = ''
          });
    }

    // Hae kaikki tentit (admin)
    // HTTP GET / localhost:5000/tentti
    var tentitInit = [] //nollaa tentit[] kun sovellus käynnistyy
    async function adminHaeKaikkiTentit() {      
        try {
            var userToken = GetUserToken()
            const response = await axios.get(url + 'tentti', { headers: {"x-access-token" : `${userToken}`} });
            console.log('Admin hae kaikki tentit...')
            console.log(response.data);
            console.log('Tentit haettu OK')
            console.log('Hae kaikki kysymykset kaikkiin tentteihin...')
            haeKysymykset(response.data)
            console.log('Kysymykset haettu tentteihin OK')
        } catch (error) {
            console.error('Käyttäjän tunnistautuminen epäonnistui, virhe tokenissa (' + error + ')');
            alert('Käyttäjän tunnistautuminen epäonnistui (virhe tokenissa). Kirjaudu sisään uudelleen.')
            handleKirjauduUlos()
        }
    }

    // Hae kaikki kysymykset tenttiin (admin ja opiskelija)
    // HTTP GET / localhost:5000/tentti/{tentti_id}/kysymykset
    async function haeKysymykset(tentitInit) {  
        tentitInit.map( (tentti, tenttiIndex) =>  {
            let kysymykset = undefined
            let tentti_id = tentti.id
            tentti.kysymykset = kysymykset
            console.log('Hae kysymykset tentti_id:lle: ' + tentitInit[tenttiIndex].id + '...')
            axios.get(url + 'tentti/' + tentti_id + '/kysymykset')
                .then(function (response) {
                    // handle success
                    console.log(response.data);
                    //tentitInit[tenttiIndex].kysymykset.push(response.data)
                    tentitInit[tenttiIndex].kysymykset = response.data
                    console.log('tentti_id: ' + tentitInit[tenttiIndex].id + ' kysymykset ' + response.data.length + ' kpl haettu ok')
                    //dispatch({tapahtumatyyppi: 'initialize', data: tentitInit}) FINALLYYN SIIRRETTY
                })
                .catch(function (error) {
                    // handle error
                    console.log('tentti_id: ' + tentitInit[tenttiIndex].id + ' virhe kysymysten haussa: ')
                    console.log(error);
                    alert('Virhe tenttien kysymysten hakemisessa. Kirjaudu sisään uudelleen.')
                    handleKirjauduUlos()
                })
                .finally(function () {
                    // always executed
                    dispatch({tapahtumatyyppi: 'initialize', data: tentitInit})
                    // TÄSSÄ VASTAUSTEN HAKU
                    console.log('Hae tenttien kysymysten vastaukset...')
                    haeVastaukset(tentitInit)
                });
            return tentti                    
        }) // tentitInit map loppu
    }

    // Hae kaikki vastaukset kysymyksiin (admin ja opiskelija)
    // HTTP GET / localhost:5000/vastaus/{kysymys_id}
    function haeVastaukset(tentitInit) {      
        for(let tenttiIndex=0; tenttiIndex < tentitInit.length; tenttiIndex++) {  
            if (tentitInit[tenttiIndex].kysymykset !== undefined) { // onko kysymyksiä tälle tentille?
                for(let kysymysIndex=0; kysymysIndex < tentitInit[tenttiIndex].kysymykset.length; kysymysIndex++) {
                    let vastaukset = undefined
                    console.log(tentitInit[tenttiIndex].kysymykset[kysymysIndex].id)
                    let kysymys_id = tentitInit[tenttiIndex].kysymykset[kysymysIndex].id
                    tentitInit[tenttiIndex].kysymykset[kysymysIndex].vastaukset = vastaukset
                    axios.get(url + 'vastaus/' + kysymys_id)
                        .then(function (response) {
                            // handle success
                            console.log(response.data);
                            //tentitInit[tenttiIndex].kysymykset[kysymysIndex].vastaukset.push(response.data)
                            tentitInit[tenttiIndex].kysymykset[kysymysIndex].vastaukset = response.data // response tulee taulukkona
                            console.log(tentitInit)
                            dispatch({tapahtumatyyppi: 'initialize', data: tentitInit})
                        })
                        .catch(function (error) {
                            // handle error
                            console.log(error);
                            alert('Virhe tenttien kysymysten/vastauksien hakemisessa. Kirjaudu sisään uudelleen.')
                            handleKirjauduUlos()
                        })
                        .finally(function () {
                            // always executed
                            //dispatch({tapahtumatyyppi: 'initialize', data: tentitInit}) SIIRRETTY .then:iin
                            haeOpiskelijanVastaukset(tentitInit)
                            haeKysymyksenAihepiirinNimi(tentitInit)
                        });  
                }
            } // if loppu
        } //tentitInit for loppu
    }

    // Hae kaikkiin vastauksiin opiskelijan vastaus (opiskelija)
    // HTTP GET / localhost:5000/vastaus/{kysymys_id}
    function haeOpiskelijanVastaukset(tentitInit) {   
        let kayttaja_id = kayttaja // useState-muuttujasta kayttaja_id

        for(let tenttiIndex=0; tenttiIndex < tentitInit.length; tenttiIndex++) {  
            if (tentitInit[tenttiIndex].kysymykset !== undefined) { // onko kysymyksiä tälle tentille?
                for(let kysymysIndex=0; kysymysIndex < tentitInit[tenttiIndex].kysymykset.length; kysymysIndex++) {
                    console.log(tentitInit[tenttiIndex].kysymykset[kysymysIndex].id)
                    // onko vastauksia tälle kysymykselle?
                    if (tentitInit[tenttiIndex].kysymykset[kysymysIndex].vastaukset !== undefined) { 
                        // luuppaa kysymyksen vastaukset
                        for(let vastausIndex=0; vastausIndex < tentitInit[tenttiIndex].kysymykset[kysymysIndex].vastaukset.length; vastausIndex++) {
                            let vastaus_id = tentitInit[tenttiIndex].kysymykset[kysymysIndex].vastaukset[vastausIndex].id
                            // localhost:5000/opiskelijaVastaus/{kayttaja_id}&{vastaus_id}
                            var userToken = GetUserToken()
                            axios.get(url + 'opiskelijaVastaus/' + kayttaja_id + '&' + vastaus_id, { headers: {"x-access-token" : `${userToken}`} } )
                                .then(function (responseGetInit) {
                                    if (responseGetInit.data.length !== 0) { // löytyy dataa, aseta vastaus käyttäjälle
                                        console.log('Vastaus: ' + vastaus_id + ' arvo: ' + responseGetInit.data[0].kayttajan_vastaus + ' asetettu käyttäjälle: ' + kayttaja_id)
                                        tentitInit[tenttiIndex].kysymykset[kysymysIndex].vastaukset[vastausIndex].kayttajan_vastaus = responseGetInit.data[0].kayttajan_vastaus
                                    } else {
                                        tentitInit[tenttiIndex].kysymykset[kysymysIndex].vastaukset[vastausIndex].kayttajan_vastaus = false
                                    }
                                    console.log(tentitInit)
                                    dispatch({tapahtumatyyppi: 'initialize', data: tentitInit})                                 
                                })
                                .catch(function (error) {
                                    console.log(error)
                                    // älä alerttaa tässä kohtaa!!!
                                    //alert('Virhe tenttien kysymysten/vastausten hakemisessa. Kirjaudu sisään uudelleen.')
                                    //handleKirjauduUlos()
                                })
                        } // for loppu vastauksille
                    } // if loppu
                } // kysymykset for loppu
            } // if loppu
        } //tentitInit for loppu
    } // funktion loppu

    // Hae kaikki tentit (opiskelija)
    // HTTP GET / localhost:5000/opiskelijaTentittavatTentit/{kayttaja_id}
    async function opiskelijaHaeTentittavatTentit(kayttaja_id) {
        try {
            var userToken = GetUserToken()
            const response = await axios.get(url + 'opiskelijaTentittavatTentit/' + kayttaja_id, { headers: {"x-access-token" : `${userToken}`} });
            console.log(response.data);
            haeKysymykset(response.data)
        } catch (error) {
            console.error('Käyttäjän tunnistautuminen epäonnistui, virhe tokenissa (' + error + ')');
            alert('Käyttäjän tunnistautuminen epäonnistui (virhe tokenissa). Kirjaudu sisään uudelleen.')
            handleKirjauduUlos()
        } finally {
            //
        }
    }

    // tentit-napin painaminen AppBarissa
    const handleNaytaTentit = () => {
        console.log("Tentit-nappia painettu.")
        setValittuTentti(null)
        setNaytaVastaukset(false)
        // ADMIN vai OPISKELIJA:
        if(onkoAdmin === true) {
            adminHaeKaikkiTentit()
            // HAE TÄSSÄ MYÖS KAIKKI AIHEPIIRIT
            adminHaeKaikkiAihepiirit()
            // HAE TÄSSÄ MYÖS KAIKKI TENTIN ADMIN-TILAT
            adminHaeKaikkiTentinTilat()
            // HAE TÄSSÄ MYÖS KAIKKI TENTIN ARVOSTELUASTEIKOT
            adminHaeKaikkiTenttiAsteikot()
        } else {
            opiskelijaHaeTentittavatTentit(kayttaja) // parametri useState-muuttujasta kayttaja_id
        }
    }

    // Hae kaikki aihepiirit (Admin)
    // HTTP GET / localhost:5000/kaikkiAihepiirit
    async function adminHaeKaikkiAihepiirit() {
        try {
            const response = await axios.get(url + 'kaikkiAihepiirit');
            console.log('Kaikki aihepiirit:')
            console.log(response.data);
            setAihePiirit(response.data)
        } catch (error) {
            console.error(error);
        }
    }

    // Hae kaikki tentin tilat (Admin)
    // HTTP GET / localhost:5000/kaikkiTenttiTilat
    async function adminHaeKaikkiTentinTilat() {
        try {
            const response = await axios.get(url + 'kaikkiTenttiTilat');
            console.log('Kaikki tentin tilat:')
            console.log(response.data);
            setTentinTilat(response.data)
        } catch (error) {
            console.error(error);
        }
    }

    // Hae kaikki tentin arvosteluasteikot (Admin)
    // HTTP GET / localhost:5000/kaikkiTenttiAsteikot
    async function adminHaeKaikkiTenttiAsteikot() {
        try {
            const response = await axios.get(url + 'kaikkiTenttiAsteikot');
            console.log('Kaikki tentin arvosteluasteikot:')
            console.log(response.data);
            setTentinArvosteluAsteikot(response.data)
        } catch (error) {
            console.error(error);
        }
    }

    // Admin: tentin admin-tilan valintalistakentän vaihtoehtojen mäppäys
    function tentinTilaRows(tentinTilat) {
        let tentinTilaRivit = tentinTilat.map((tentinTila) => {
            let tentinTilaRivi = <MenuItem key={tentinTila.id} value={tentinTila.id}>{tentinTila.tila}</MenuItem>
            return (tentinTilaRivi)
        })
        return tentinTilaRivit
    }

    // Admin: tentin arvosteluasteikkojen valintalistakentän vaihtoehtojen mäppäys
    function tentinArvosteluAsteikotRows(tentinArvosteluAsteikot) {
        let tentinArvosteluAsteikotRivit = tentinArvosteluAsteikot.map((tentinArvosteluAsteikko) => {
            let tentinArvosteluAsteikkoRivi = <MenuItem key={tentinArvosteluAsteikko.asteikko_id} value={tentinArvosteluAsteikko.asteikko_id}>{tentinArvosteluAsteikko.asteikko_id}</MenuItem>
            return (tentinArvosteluAsteikkoRivi)
        })
        return tentinArvosteluAsteikotRivit
    }

    // Hae kaikkiin kysymyksiin aihepiiri tekstinä aihepiiri_id:n avulla
    // HTTP GET / localhost:5000/kysymyksenAihepiiri/{aihepiiri_id} 
    function haeKysymyksenAihepiirinNimi(tentitInit) {
        for(let tenttiIndex=0; tenttiIndex < tentitInit.length; tenttiIndex++) {  
            if (tentitInit[tenttiIndex].kysymykset !== undefined) { // onko kysymyksiä tälle tentille?
                for(let kysymysIndex=0; kysymysIndex < tentitInit[tenttiIndex].kysymykset.length; kysymysIndex++) {
                    console.log(tentitInit[tenttiIndex].kysymykset[kysymysIndex].id)                  
                    let aihepiiri_id = tentitInit[tenttiIndex].kysymykset[kysymysIndex].aihepiiri_id
                    // var userToken = GetUserToken() EI TARTTE
                    // ei tartte tokenia: { headers: {"x-access-token" : `${userToken}`} }
                    // localhost:5000/kysymyksenAihepiiri/{aihepiiri_id} 
                    axios.get(url + 'kysymyksenAihepiiri/' + aihepiiri_id)                
                        .then(function (responseGetInit) {
                            if (responseGetInit.data.length !== 0) { // löytyy dataa
                                console.log('Aihepiiri tekstinä: ' + responseGetInit.data[0].nimi)
                                tentitInit[tenttiIndex].kysymykset[kysymysIndex].aihepiiri_nimi = responseGetInit.data[0].nimi
                            } else {
                                tentitInit[tenttiIndex].kysymykset[kysymysIndex].aihepiiri_nimi = "Aihepiiriä ei löytynyt"
                            }
                            console.log(tentitInit)
                            dispatch({tapahtumatyyppi: 'initialize', data: tentitInit})                                 
                        })
                        .catch(function (error) {
                            console.log(error)
                            // älä alerttaa tässä kohtaa!!!
                            //alert('Virhe tenttien kysymysten/vastausten hakemisessa. Kirjaudu sisään uudelleen.')
                            //handleKirjauduUlos()
                        })                   
                } // kysymykset for loppu
            } // if loppu
        } //tentitInit for loppu
    } // funktion loppu

// ********************************************************************************************


// AXIOS:in KÄYTTÖ
    // useEffect käynnistyy, kun tentit-taulukko päivittyy
    useEffect( () => {

        // tallenna localStorageen vain yksi JSON-objekti(merkkijonona), joka päivittyy tarpeen mukaan
        // tentit on taulukko-olio, jota setItem ei osaa tallettaa localStorageen.
        // Siksi käytettävä JSON.stringify() -funktiota, joka muuntaa objektin ensin string-muotoon:
        if (!komponenttiAlustettu) {
            if (!localStorage.getItem("tenttiappTentit")) {
                localStorage.setItem("tenttiappTentit",JSON.stringify(tentit))
            } else {
                // käyttöliittymän alustaminen
               dispatch({tapahtumatyyppi: 'initialize', data: tentit})
                //dispatch({tapahtumatyyppi: 'initialize', data: JSON.parse(localStorage.getItem("tenttiappTentit"))})
            }
            setKomponenttiAlustettu(true);
        }
        else {
            localStorage.setItem("tenttiappTentit", JSON.stringify(tentit));
        }     

    },[tentit]);


// ALKUPERÄINEN LOCALSTORAGEN KAUTTA TOIMIVA ALLA:
/*
    // useEffect käynnistyy, kun tentit-taulukko päivittyy
    useEffect( () => {

        // tallenna localStorageen vain yksi JSON-objekti(merkkijonona), joka päivittyy tarpeen mukaan
        // tentit on taulukko-olio, jota setItem ei osaa tallettaa localStorageen.
        // Siksi käytettävä JSON.stringify() -funktiota, joka muuntaa objektin ensin string-muotoon:
        if (!komponenttiAlustettu) {
            if (!localStorage.getItem("tenttiappTentit")) {
                localStorage.setItem("tenttiappTentit",JSON.stringify(tentit))
            } else {
                dispatch({tapahtumatyyppi: 'initialize', data: JSON.parse(localStorage.getItem("tenttiappTentit"))})
                //vanha: setTentit(JSON.parse(localStorage.getItem("tenttiappTentit")))
            }
            setKomponenttiAlustettu(true);
        }
        else {
            localStorage.setItem("tenttiappTentit", JSON.stringify(tentit));
        }     

    },[tentit]);

*/

    // TenttiApp-piirto
    return (
        <div>
            <div>
                <ButtonAppBar   kayttaja={kayttaja}
                                kayttajaNimi={kayttajaNimi}
                                onkoAdmin={onkoAdmin}
                                handleKirjauduSisaan={handleKirjauduSisaan}
                                handleKirjauduUlos={handleKirjauduUlos}
                                handleNaytaTentit={handleNaytaTentit}
                                //VANHA: handleAddNewExamAdmin={handleAddNewExamAdmin}
                                dispatch={dispatch}
                                setValittuTentti={setValittuTentti}
                                valittuTentti={valittuTentti}
                                tentit={tentit}
                                tenttiLength={tentit.length}
                                url={url}
                                setOnkoTentitHaettu={setOnkoTentitHaettu} 
                ></ButtonAppBar>
            </div>
            <div>
                {
                    kayttaja === null && <LogIn handleKirjauduSisaan={handleKirjauduSisaan} // vanha
                                                handleOpiskelijaRekisteroidy={handleOpiskelijaRekisteroidy}
                                                handleKirjauduSisaanUUSI={handleKirjauduSisaanUUSI}
                                         ></LogIn>
                }
            </div>
            <div>
                {kayttaja !== null && tentit.length > 0 && valittuTentti === null &&  // piilota jos joku tentti valittu
                // tenttien sorttaustoiminto: 
                <TenttiSort aakkosjärjestys={aakkosjärjestys}
                            setAakkosjärjestys={setAakkosjärjestys}
                            setValittuTentti={setValittuTentti}
                            kayttaja={kayttaja}
                            onkoAdmin={onkoAdmin}>
                </TenttiSort>
                }

                {kayttaja !== null && valittuTentti === null &&  // piilota jos joku tentti valittu
                // tenttien näyttäminen
                tentit.map((tentti, i) => <Button key={i.toString()} variant="text" color="primary"
                    onClick={() =>  {setValittuTentti(i)
                                    setNaytaVastaukset(false)}
                            }>{ tentti.nimi!=="" ? tentti.nimi : "[Nimetön tentti]"}</Button>
                )}
            </div>

            <Box>
                {kayttaja !== null && tentit.length === 0 && onkoTentitHaettu === false &&
                <TextField        
                    className={classes.textField}          
                    disabled 
                    style={{ marginLeft: 10, marginTop: 10, fontSize: 30 }}
                    id="hae-tentit-info-standard-disabled" 
                    label={'Hae tentit info'}
                    defaultValue={"Hae tentit painamalla TENTIT-nappia toimintopalkissa."}
                    variant="standard"
                />              
                }
            </Box>

            <Box>
                {kayttaja !== null && tentit.length === 0 && onkoTentitHaettu === true &&
                <TextField        
                    className={classes.textField}          
                    disabled 
                    style={{ marginLeft: 10, marginTop: 10, fontSize: 30 }}
                    id="hae-tentit-info-standard-disabled-haettu" 
                    label={'Hae tentit info'}
                    defaultValue={"TENTTEJÄ EI LÖYTYNYT."}
                    variant="standard"
                />              
                }
            </Box>

            <div>
                {valittuTentti !== null && onkoAdmin === false && tentit[valittuTentti].kysymykset.map( (kysymys, kysymysIndex) =>
                    <Kysymys key={kysymysIndex}
                            kysymysIndex={kysymysIndex} 
                            {...kysymys} 
                            tenttiIndex={valittuTentti}
                            tenttiNimi={tentit[valittuTentti].nimi}
                            dispatch={dispatch}
                            //VANHA: action={handleCheckboxChange}
                            naytaVastaukset={naytaVastaukset}
                            url={url}
                            kayttaja_id={kayttaja} // kayttaja_id-props useState-muuttujasta kayttaja
                    />)}
            </div>
         
            <Box>               
                {valittuTentti !== null && onkoAdmin === true &&
                <DialogAlertExamDelete  //VANHA: handleDeleteExamAdmin={handleDeleteExamAdmin}
                                        dispatch={dispatch}
                                        tenttiIndex={valittuTentti}
                                        setValittuTentti={setValittuTentti}
                                        tentti_id={tentit[valittuTentti].id}
                                        url={url}
                />
                }
            </Box>
          
            <Box>
                {valittuTentti !== null && onkoAdmin === true &&
                <TextField
                    required 
                    id={valittuTentti + tentit[valittuTentti].nimi}
                    label={"Muokkaa tentin nimeä"}
                    //defaultValue={tentit[valittuTentti].nimi}
                    value={tentit[valittuTentti].nimi} // käytetään tässä onChangea ks. alle
                    className={classes.textField}
                    helperText=""
                    margin="normal"
                    variant="filled"
                    // VANHA: onChange={(event) => { document.getElementById(valittuTentti + tentit[valittuTentti].nimi).value =                        
                    //          handleExamNameChangeAdmin(event, valittuTentti) }}
                    onChange={(event, currentValue) => { document.getElementById(valittuTentti + tentit[valittuTentti].nimi).value = 
                                // SQL-haku: PUT / localhost:5000/tentti/{tentti_id}, data bodyssä
                                currentValue = event.target.value
                                var userToken = GetUserToken()
                                // muista: Body menee ennen Headeria axios-pyynnössä:
                                axios.put(url + 'tentti/' + tentit[valittuTentti].id , {
                                    nimi: currentValue
                                  } , { headers: {"x-access-token" : `${userToken}`} } )
                                  .then(function (response) {
                                    console.log(response);
                                    dispatch(
                                        {   nimi: "examNameChangeAdmin",
                                            //arvo: event.target.value,
                                            arvo: currentValue,
                                            tapahtumatyyppi: 'muuta',
                                            tenttiIndex: valittuTentti,
                                            kysymysIndex: undefined,
                                            vastausIndex: undefined,
                                            id: response.data[0].id 
                                        }
                                    )
                                  })
                                  .catch(function (error) {
                                    console.error('Tentin nimen talletus epäonnistui (' + error + ')');
                                    alert('Tentin nimen talletus epäonnistui. Kirjaudu sisään uudelleen.')
                                    handleKirjauduUlos()
                                  });
                                /*
                                dispatch(
                                    {   nimi: "examNameChangeAdmin",
                                        arvo: event.target.value,
                                        tapahtumatyyppi: 'muuta',
                                        tenttiIndex: valittuTentti,
                                        kysymysIndex: undefined,
                                        vastausIndex: undefined 
                                    }
                                )
                                */
                                }
                            }
                /> }

                {valittuTentti !== null && onkoAdmin === true &&
                <FormControl variant="filled" className={classes.formControl}>
                    <InputLabel id={valittuTentti + tentit[valittuTentti].tentintila_admin_id}>Tentin tila</InputLabel>
                    <Select
                        labelId={valittuTentti + tentit[valittuTentti].tentintila_admin_id}
                        id={valittuTentti + tentit[valittuTentti].tentintila_admin_id}
                        value={tentit[valittuTentti].tentintila_admin_id}
                        onChange={(event, currentValue) => { document.getElementById(valittuTentti + tentit[valittuTentti].tentintila_admin_id).value = 
                            // SQL-kysely: PUT / localhost:5000/tentti/{tentti_id}
                            currentValue = event.target.value
                            var userToken = GetUserToken()
                            axios.put(url + 'tentti/' + tentit[valittuTentti].id , {
                                tentintila_admin_id: currentValue
                                } , { headers: {"x-access-token" : `${userToken}`} } )
                                .then(function (response) {
                                    console.log(response);
                                    dispatch(
                                        {   nimi: "examStatusChangeAdmin",
                                            arvo: currentValue,
                                            tapahtumatyyppi: 'muuta',
                                            tenttiIndex: valittuTentti,
                                            kysymysIndex: undefined,
                                            vastausIndex: undefined
                                        }
                                    )
                                })
                                .catch(function (error) {
                                    console.error('Tentin tilan talletus epäonnistui (' + error + ')');
                                    alert('Tentin tilan talletus epäonnistui. Kirjaudu sisään uudelleen.')
                                    handleKirjauduUlos()
                                });                          
                            }                   
                        }
                    >
                        {   // hae tentin admin-tilat valintalistaan:
                        tentinTilaRows(tentinTilat)
                        }                       
                    </Select>
                </FormControl> }

                {valittuTentti !== null && onkoAdmin === true &&
                <FormControl variant="filled" className={classes.formControl}>
                    <InputLabel id={valittuTentti + tentit[valittuTentti].asteikko_id}>Arvosteluasteikko</InputLabel>
                    <Select
                        labelId={valittuTentti + tentit[valittuTentti].asteikko_id}
                        id={valittuTentti + tentit[valittuTentti].asteikko_id}
                        value={tentit[valittuTentti].asteikko_id}
                        onChange={(event, currentValue) => { document.getElementById(valittuTentti + tentit[valittuTentti].asteikko_id).value = 
                            // SQL-kysely: PUT / localhost:5000/tentti/{tentti_id}
                            currentValue = event.target.value
                            var userToken = GetUserToken()
                            axios.put(url + 'tentti/' + tentit[valittuTentti].id , {
                                asteikko_id: currentValue
                                } , { headers: {"x-access-token" : `${userToken}`} } )
                                .then(function (response) {
                                    console.log(response);
                                    dispatch(
                                        {   nimi: "examGradingScaleChangeAdmin",
                                            arvo: currentValue,
                                            tapahtumatyyppi: 'muuta',
                                            tenttiIndex: valittuTentti,
                                            kysymysIndex: undefined,
                                            vastausIndex: undefined
                                        }
                                    )
                                })
                                .catch(function (error) {
                                    console.error('Tentin tilan talletus epäonnistui (' + error + ')');
                                    alert('Tentin tilan talletus epäonnistui. Kirjaudu sisään uudelleen.')
                                    handleKirjauduUlos()
                                });                          
                            }                   
                        }
                    >
                        {   // hae tentin admin-tilat valintalistaan:
                        tentinArvosteluAsteikotRows(tentinArvosteluAsteikot)
                        }                       
                    </Select>
                </FormControl> }

            </Box>
            
            <div>
                {valittuTentti !== null && onkoAdmin === true && tentit[valittuTentti].kysymykset.map( (kysymys, kysymysIndex) =>
                    <Admin  key={kysymysIndex}
                            kysymysIndex={kysymysIndex} 
                            {...kysymys} 
                            tenttiIndex={valittuTentti}
                            tenttiNimi={tentit[valittuTentti].nimi}
                            dispatch={dispatch}
                            url={url}
                            kysymys_id={kysymys.id}
                            tentti_id={tentit[valittuTentti].id}
                            aihePiirit={aihePiirit}
                            handleKirjauduUlos={handleKirjauduUlos}
                            // VANHAT TILAMUUTOSKÄSITTELIJÄT:
                            //handleQuestionChangeAdmin={handleQuestionChangeAdmin}
                            //handleCheckboxChangeAdmin={handleCheckboxChangeAdmin}
                            //handleAnswerChangeAdmin={handleAnswerChangeAdmin}
                            //handleQuestionCategoryChangeAdmin={handleQuestionCategoryChangeAdmin}
                            //handleAddNewAnswerAdmin={handleAddNewAnswerAdmin}
                            //handleDeleteAnswerAdmin={handleDeleteAnswerAdmin}
                            //handleDeleteQuestionAdmin={handleDeleteQuestionAdmin}
                            //handleAddNewQuestionAdmin={handleAddNewQuestionAdmin}                           
                            //handleExamNameChangeAdmin={handleExamNameChangeAdmin}                                                                      
                    />)}
            </div>
           
            <Box>
                <p><br /><br /></p>
                {valittuTentti !== null && onkoAdmin === true &&
                <TextField        
                    className={classes.textField}          
                    disabled 
                    style={{ marginLeft: 10, marginTop: 10, fontSize: 30 }}
                    id="lisaa_kysymys_loppuun-standard-disabled" 
                    label={tentit[valittuTentti].nimi}
                    defaultValue={"Lisää uusi kysymys"}
                    variant="standard"
                />              
                }
            
            
                {valittuTentti !== null && onkoAdmin === true &&
                    <AddCircleOutlineRoundedIcon 
                    style={{ color: green[500], marginLeft: 10, marginTop: 20, fontSize: 30 }}
                    //VANHA: onClick={(event) => handleAddNewQuestionAdmin(event, valittuTentti, tentit[valittuTentti].kysymykset.length +1) }
                    onClick={() => {
                        // SQL-haku: POST / localhost:5000/kysymys/{tentti_id}
                        var userToken = GetUserToken()
                        axios.post(url + 'kysymys/' + tentit[valittuTentti].id , {
                            // ei bodyssä mitään tartte viedä
                            } , { headers: {"x-access-token" : `${userToken}`} } )
                            .then(function (response) {
                                console.log(response);
                                console.log('kysymys_id: ' + response.data[0].id)
                                dispatch(
                                    {   nimi: "questionAddAdmin",
                                        arvo: undefined,
                                        tapahtumatyyppi: 'lisää',
                                        tenttiIndex: valittuTentti,
                                        kysymysIndex: tentit[valittuTentti].kysymykset.length +1,
                                        vastausIndex: undefined,
                                        kysymys_id: response.data[0].id
                                    }
                                )
                            })
                            .catch(function (error) {
                                console.error('Kysymyksen lisääminen epäonnistui (' + error + ')');
                                alert('Kysymyksen lisääminen epäonnistui. Kirjaudu sisään uudelleen sovellukseen, mikäli virhe toistuu.')
                                //handleKirjauduUlos()
                            })
                        }
                        /* VANHA:
                        dispatch(
                            {nimi: "questionAddAdmin",
                            arvo: undefined,
                            tapahtumatyyppi: 'lisää',
                            tenttiIndex: valittuTentti,
                            kysymysIndex: tentit[valittuTentti].kysymykset.length +1,
                            vastausIndex: undefined }
                        )
                        */                                                                
                    }
                    />
                }
            </Box>
            
            <div>
                {valittuTentti !== null && onkoAdmin === false && naytaVastaukset === false && <Button variant="contained" color="primary"
                    onClick={() => setNaytaVastaukset(true) }>Näytä vastaukset</Button>}
            </div>
            <div>
                {valittuTentti !== null && onkoAdmin === false && naytaVastaukset === true && <Button variant="contained" color="primary"
                    onClick={() => setNaytaVastaukset(false) }>Piilota vastaukset</Button>}
            </div>

            <div>
                {valittuTentti !== null && naytaVastaukset === true &&
                <Stats  data={tentit[valittuTentti].kysymykset}
                ></Stats>}
            </div>
            
        </div>
    );
}

export default TenttiApp;