import React, {useReducer} from 'react'; // useState ei tartte
//import testdata from './testdata'; // ENNEN AXIOSTA

//const alkutila = testdata; // ENNEN AXIOSTA
const alkutila = [];
const TenttiContext = React.createContext(alkutila);

// tila vastaa 'tentit' -useState-muuttujan arvoa
const reducer = (tila, toiminto) => {

    // MUUTA-TOIMINNOT
    if (toiminto.tapahtumatyyppi === 'muuta') {
        let nimi = toiminto.nimi; // mikä toiminto kyseessä
        let arvo = toiminto.arvo; // uusi toiminnon kohteen tila
        let tenttiIndex = toiminto.tenttiIndex;
        let kysymysIndex = toiminto.kysymysIndex;
        let vastausIndex = toiminto.vastausIndex; // HUOM: voidaan käsitellä spreadillä, FixMe!
        let nykyinenData = JSON.parse(JSON.stringify(tila)) // TAI: let nykyinenTila = [...tila]
        // käyttäjä muuttaa vastausruksia (checkbox):
        if (nimi==="checkboxChangeUser") {
            nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[vastausIndex].kayttajan_vastaus = arvo; // oli: .valittu
        // admin muuttaa oikeaa vastausruksia (checkbox):
        } else if (nimi==="checkboxChangeAdmin") {
            nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[vastausIndex].oikein = arvo; // oli: .oikeavastaus
        // admin muuttaa kysymyksen sisältöä (text field):
        } else if (nimi=== "questionChangeAdmin") {
            nykyinenData[tenttiIndex].kysymykset[kysymysIndex].teksti = arvo; // oli: .kysymys
        // admin muuttaa kysymyksen aihepiiriä (valintalista):
        } else if (nimi==="questionCategoryChangeAdmin") {
            nykyinenData[tenttiIndex].kysymykset[kysymysIndex].aihepiiri_id = arvo; // oli: .aihepiiri
        // admin muuttaa vastauksen sisältöä (text field):
        } else if (nimi=== "answerChangeAdmin") {
            nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[vastausIndex].teksti = arvo; // oli: .vastaus
        // admin muuttaa tentin nimeä (text field):
        } else if (nimi==="examNameChangeAdmin") {
            nykyinenData[tenttiIndex].nimi = arvo;
        // admin muuttaa tentin tilaa (valintalista):
        } else if (nimi==="examStatusChangeAdmin") {
            nykyinenData[tenttiIndex].tentintila_admin_id = arvo;
        // admin muuttaa tentin arvosteluasteikkoa (valintalista):
        } else if (nimi==="examGradingScaleChangeAdmin") {
            nykyinenData[tenttiIndex].asteikko_id = arvo;
        }
        
        return nykyinenData;

    // POISTA TOIMINNOT
    } else if (toiminto.tapahtumatyyppi === 'poista') {
        let nimi = toiminto.nimi; // mikä toiminto kyseessä
        //let arvo = toiminto.arvo; // uusi toiminnon kohteen tila
        let tenttiIndex = toiminto.tenttiIndex;
        let kysymysIndex = toiminto.kysymysIndex;
        let vastausIndex = toiminto.vastausIndex; // HUOM: voidaan käsitellä spreadillä, FixMe!
        let nykyinenData = JSON.parse(JSON.stringify(tila)) // TAI: let nykyinenTila = [...tila]
        // admin poistaa vastauksen (vastauksen roskisnapit):
        if (nimi==="deleteAnswerAdmin") {
            nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset.splice(vastausIndex, 1)
        } 
        // admin poistaa kysymyksen (kysymyksen roskisnapit):
        else if (nimi==="deleteQuestionAdmin") {
            nykyinenData[tenttiIndex].kysymykset.splice(kysymysIndex, 1)
        }
        // admin poistaa tentin ('poista tentti' roskisnappi): EI KÄYTÖSSÄ NYT TÄSSÄ REDUCERISSA
        else if (nimi==="examDeleteAdmin") {
            nykyinenData.splice(tenttiIndex, 1)
        }

        return nykyinenData;

    // LISÄÄ TOIMINNOT
    } else if (toiminto.tapahtumatyyppi=== 'lisää') {

        let nimi = toiminto.nimi; // mikä toiminto kyseessä
        //let arvo = toiminto.arvo; // uusi toiminnon kohteen tila
        //let id = toiminto.id // kysymys- tai vastaus id (SQL:n palauttama)
        let tentti_id = toiminto.tentti_id
        let kysymys_id = toiminto.kysymys_id
        let vastaus_id = toiminto.vastaus_id
        let tenttiIndex = toiminto.tenttiIndex;
        let kysymysIndex = toiminto.kysymysIndex;
        let vastausIndex = toiminto.vastausIndex; // HUOM: voidaan käsitellä spreadillä, FixMe!
        let nykyinenData = JSON.parse(JSON.stringify(tila)) // TAI: let nykyinenTila = [...tila]
        // admin lisää uuden vastauksen (vastauksen '+'-napit):
        if (nimi==="answerAddAdmin") {
            // esim. {vastaus: "Maapallon ja ulkoavaruuden ilmiöitä", valittu: false, oikeavastaus: false}
            // OLI: let uusiVastaus = {vastaus: "", valittu: false, oikeavastaus: false}
            // SQL: post "Kayttajien_vastaukset"-tauluun uusi, post "Vastaus"-tauluun uusi, myös id:t
            let uusiVastaus = { id: vastaus_id, // TYHJÄ NYT
                                teksti: "",
                                kysymys_id: kysymys_id,
                                kayttajan_vastaus: false,
                                oikein: false}

            if (vastausIndex !== undefined) {
                nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset.splice(vastausIndex, 0, uusiVastaus)
            } else {
                nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset.push(uusiVastaus)
            }
        }
        // admin lisää uuden kysymyksen (kysymyksen '+'-napit)
        else if (nimi==="questionAddAdmin") {
            /* oli ennen
            let uusiKysymys = { kysymys: "", 
                                aihepiiri: "", 
                                vastaukset: [
                                    {vastaus: "", valittu: false, oikeavastaus: false}
                            ] } 
            */
           // SQL: post "Kysymys"-tauluun, myös id ja tentti_id
           let uusiKysymys = {  id: kysymys_id,
                                teksti: "", 
                                aihepiiri_id: "", 
                                tentti_id: nykyinenData[tenttiIndex].id,
                                vastaukset: [
                                   // EI LISÄTÄ VASTAUSTA {vastaus: "", valittu: false, oikeavastaus: false}
                            ] } 
            nykyinenData[tenttiIndex].kysymykset.splice(kysymysIndex, 0, uusiKysymys)
        }
        // admin lisää uuden tentin (AppBarin 'lisää tentti' -toimintonappi):
        else if (nimi=== "examAddAdmin") {
            /* oli ennen:
            let uusiTentti = {
                nimi: "",
                kysymykset: []
            }
            */
           // SQL: post "Tentti"-tauluun, myös id
            let uusiTentti = {
                id: tentti_id,
                nimi: "",
                tentintila_admin_id: 1,
                asteikko_id: 1,
                kysymykset: []
            }
            nykyinenData.splice(tila.length, 0, uusiTentti)
        }

        return nykyinenData;

    // TENTTIEN SORTTAUSTOIMINTO:
    } else if (toiminto.tapahtumatyyppi=== 'examSort') {
        let järjestys = toiminto.järjestys
        let nykyinenData = JSON.parse(JSON.stringify(tila))
        if (järjestys === 'aToz') {
            nykyinenData.sort( (a, b) => a.nimi.localeCompare(b.nimi, 'fi', {ignorePunctuation:true})<0 ? -1 : 1 )
        } else if (järjestys==='zToa') {
            nykyinenData.sort( (a, b) => a.nimi.localeCompare(b.nimi, 'fi', {ignorePunctuation:true})<0 ? 1 : -1 )
        }
        return nykyinenData

    // ALKUDATAN asettaminen: kun komponentti ei ole vielä alustettu:
    } else if (toiminto.tapahtumatyyppi=== 'initialize') {
        return toiminto.data

    // PALAUTA MUUTOIN VIRHE:
    } else {
        throw new Error();
    }

}
//************************************ */

// TenttiProvider:
function TenttiProvider(props) {

    const [tentit, dispatch] = useReducer(reducer, alkutila) // reducer-funktio

    return (
        <TenttiContext.Provider value={ {tentit, dispatch} }>
        {props.children}
        </TenttiContext.Provider>
    );

}

export { TenttiContext, TenttiProvider };