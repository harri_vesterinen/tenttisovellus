import React, { useState, useEffect } from 'react';
import kuvaPepsu from './images/Pepsu_1.jpg';
import testdata from './testdata';
import './App.css';

import { Box, Button, Checkbox, FormLabel } from '@material-ui/core'; // Container poistettu
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/core/styles'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { green, blue } from '@material-ui/core/colors'; // red poistettu
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import AddCircleOutlineRoundedIcon from '@material-ui/icons/AddCircleOutlineRounded';
//import DeleteForeverRoundedIcon from '@material-ui/icons/DeleteForeverRounded';
//import { maxWidth, minWidth } from '@material-ui/system';
import ButtonAppBar from './components/AppBar/ButtonAppBar'

import LogIn from './components/LogIn/LogIn'
import Stats from './components/Stats/Stats'
import Admin from './components/Admin/Admin'
import DialogAlertExamDelete from './components/Dialogs/DialogAlertExamDelete'
//import { maxWidth } from '@material-ui/system';

// ********************************

// ********** geneerinen tapahtumankäsittelijä: ************
//toiminto ={nimi: a, arvo: b, tapahtumatyyppi: c}
//toiminto ={nimi: a, arvo: b, tapahtumatyyppi: 'poista'}
//toiminto ={nimi: a, arvo: b, tapahtumatyyppi: 'lisää'}
//toiminto ={nimi: a, arvo: b, tapahtumatyyppi: 'muuta'}

// tila vastaa 'tentit' -useState-muuttujan arvoa
const handleTenttiChange = (tila, toiminto) => {

    if (toiminto.tapahtumatyyppi === 'muuta') {
        let nimi = toiminto.nimi;
        let arvo = toiminto.arvo; // uusi checkboxin tila
        let tenttiIndex = toiminto.tenttiIndex;
        let kysymysIndex = toiminto.kysymysIndex;
        let vastausIndex = toiminto.vastausIndex; // HUOM: voidaan käsitellä spreadillä, FixMe!
        let nykyinenData = JSON.parse(JSON.stringify(tila))
        nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[vastausIndex].valittu = arvo;
        return nykyinenData;
    }

}
//************************************ */



const Kysymys = (props) => {

    const [kaikkiOikein, setKaikkiOikein] = useState(true)

    //********* */
    const useStyles = makeStyles(theme => ({
        container: {
          display: 'flex',
          flexWrap: 'wrap',
          marginLeft: theme.spacing(2),
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 800,
        },
        formControl: {
            margin: theme.spacing(2),
            marginLeft: theme.spacing(1),
            minWidth: 120,
        },
        
      }));
    const classes = useStyles();
    /************ */

    // vihreä CheckBox
    const GreenCheckbox = withStyles({
        root: {
          color: green[400],
          '&$checked': {
            color: green[600],
          },
        },
        checked: {},
    })(props => <Checkbox color="default" {...props} />);

    // sininen CheckBox
    const BlueCheckbox = withStyles({
        root: {
          color: blue[400],
          '&$checked': {
            color: blue[600],
          },
        },
        checked: {},
    })(props => <Checkbox color="default" {...props} />);

    // kun painettu 'näytä vastaukset'-nappia App-komponentissa:
    useEffect( () => {
        
        setKaikkiOikein(true)
        props.vastaukset.map( vastaus => {
            if (vastaus.valittu !== vastaus.oikeavastaus) {
                setKaikkiOikein(false)
            }
            return vastaus
        } )

    }, [props.naytaVastaukset] )

    // piirto
    return (
    
        <Paper>                    
            <Box>                          
                
                <TextField
                    required
                    id={props.tenttiIndex + props.kysymysIndex + props.kysymys}
                    label={props.tenttiNimi}
                    //defaultValue={props.kysymys}
                    value={ (props.kysymysIndex + 1) + ". " + (props.kysymys) }
                    className={classes.textField}
                    helperText={props.tenttiNimi}
                    margin="none"
                    variant="filled"                 
                />

                {props.naytaVastaukset === true && kaikkiOikein === true &&
                    <img src={kuvaPepsu} width="55" height="55" alt="Oikein!" />}                   
            </Box>
                       
            {props.vastaukset.map( (vastaus, i) => (
                <Box key={i.toString()}>
                    <FormLabel>
                        <GreenCheckbox checked={vastaus.valittu} 
                            onChange={props.naytaVastaukset === false ? (event) => props.action(event, props.tenttiIndex, props.kysymysIndex, i) : undefined }>
                        </GreenCheckbox>
                    </FormLabel>
                    {props.naytaVastaukset === true &&
                    <FormControlLabel
                            control={
                                <BlueCheckbox checked={vastaus.oikeavastaus} />
                             }
                            label=""
                    />
                    }
                    {vastaus.vastaus}
                </Box>
                ))
            }

        </Paper>
    );
}

//************************************** */
const App = () => {

    // tentit voisi laittaa omaan testdata tms. tiedostoon
    const [tentit, setTentit] = useState(testdata)
    const [kayttaja, setKayttaja] = useState(null)
    const [onkoAdmin, setOnkoAdmin] = useState(false)
    const [valittuTentti, setValittuTentti] = useState(null)
    const [naytaVastaukset, setNaytaVastaukset] = useState(false)
    const [komponenttiAlustettu, setKomponenttiAlustettu] = useState(false)

    //********* */
    const useStyles = makeStyles(theme => ({
        container: {
          display: 'flex',
          flexWrap: 'wrap',
          marginLeft: theme.spacing(2),
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 700,
        },
        formControl: {
            margin: theme.spacing(2),
            marginLeft: theme.spacing(1),
            minWidth: 120,
        },
        
      }));
    const classes = useStyles();
    /************ */

    // KÄYTTÄJÄ: vastaus-checkboxin muuttaminen (käyttäjän näkymä)
    const handleCheckboxChange = (event, tenttiIndex, kysymysIndex, vastausIndex) => {      
        let nykyinenData = JSON.parse(JSON.stringify(tentit))
        nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[vastausIndex].valittu = event.target.checked
        setTentit(nykyinenData);
    }

    // ADMIN: oikea vastaus-checkboxin muuttaminen (Admin-näkymä)
    const handleCheckboxChangeAdmin = (event, tenttiIndex, kysymysIndex, vastausIndex) => {      
        let nykyinenData = JSON.parse(JSON.stringify(tentit))
        nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[vastausIndex].oikeavastaus = event.target.checked
        setTentit(nykyinenData);
    }

    // ADMIN: tentin kysymyksen tekstin muuttaminen (Admin-näkymä)
    const handleQuestionChangeAdmin = (event, tenttiIndex, kysymysIndex) => {      
        let nykyinenData = JSON.parse(JSON.stringify(tentit))
        nykyinenData[tenttiIndex].kysymykset[kysymysIndex].kysymys = event.target.value
        setTentit(nykyinenData);
    }

    // ADMIN: tentin kysymyksen aihepiirin muuttaminen (Admin-näkymä)
    const handleQuestionCategoryChangeAdmin = (event, tenttiIndex, kysymysIndex) => {      
        let nykyinenData = JSON.parse(JSON.stringify(tentit))
        nykyinenData[tenttiIndex].kysymykset[kysymysIndex].aihepiiri = event.target.value
        setTentit(nykyinenData);
    }

    // ADMIN: vastauksen tekstin muuttaminen (Admin-näkymä)
    const handleAnswerChangeAdmin = (event, tenttiIndex, kysymysIndex, vastausIndex) => {      
        let nykyinenData = JSON.parse(JSON.stringify(tentit))
        nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[vastausIndex].vastaus = event.target.value
        setTentit(nykyinenData);
    }

    // ADMIN: 'lisää uusi vastaus' -napin painaminen (Admin-näkymä)
    const handleAddNewAnswerAdmin = (event, tenttiIndex, kysymysIndex, vastausIndex) => {      
        // esim. {vastaus: "Maapallon ja ulkoavaruuden ilmiöitä", valittu: false, oikeavastaus: false}
        let uusiVastaus = {vastaus: "", valittu: false, oikeavastaus: false}
        let nykyinenData = JSON.parse(JSON.stringify(tentit))
        if (vastausIndex !== undefined) {
            nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset.splice(vastausIndex, 0, uusiVastaus)
        } else {
            nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset.push(uusiVastaus)
        }
        setTentit(nykyinenData);
        console.log("lisää uusi vastaus indeksiin " + vastausIndex + " " + JSON.stringify(uusiVastaus))
    }

    // ADMIN: 'poista vastaus' -napin painaminen (Admin-näkymä)
    const handleDeleteAnswerAdmin = (event, tenttiIndex, kysymysIndex, i) => {      
        let nykyinenData = JSON.parse(JSON.stringify(tentit))
        nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset.splice(i, 1)
        setTentit(nykyinenData);
        console.log("poista vastaus indeksistä " + i)
    }

    // ADMIN: 'lisää uusi kysymys' -napin painaminen (Admin-näkymä)
    const handleAddNewQuestionAdmin = (event, tenttiIndex, kysymysIndex) => {     
        let uusiKysymys = { kysymys: "", 
                            aihepiiri: "", 
                            vastaukset: [
                                {vastaus: "", valittu: false, oikeavastaus: false}
                            ] } 
        let nykyinenData = JSON.parse(JSON.stringify(tentit))
        nykyinenData[tenttiIndex].kysymykset.splice(kysymysIndex, 0, uusiKysymys)
        setTentit(nykyinenData);
        console.log("lisää kysymys indeksiin " + kysymysIndex)
    }

    // ADMIN: 'poista kysymys' -napin painaminen (Admin-näkymä) huom. poistaa myös vastaukset kysymyksestä
    const handleDeleteQuestionAdmin = (event, tenttiIndex, kysymysIndex) => {
        let nykyinenData = JSON.parse(JSON.stringify(tentit))
        nykyinenData[tenttiIndex].kysymykset.splice(kysymysIndex, 1)
        setTentit(nykyinenData);
        console.log("poista kysymys indeksistä " + kysymysIndex)
    }

    // ADMIN: 'uusi tentti' -napin painaminen (Admin-näkymä)
    const handleAddNewExamAdmin = (event) => {   
        let uusiTentti = {
            nimi: "",
            kysymykset: []
        }
        let nykyinenData = JSON.parse(JSON.stringify(tentit))
        nykyinenData.splice(tentit.length, 0, uusiTentti)
        setTentit(nykyinenData);
        setValittuTentti(tentit.length)
        console.log("lisää tentti indeksiin " + tentit.length)
    }

     // ADMIN: 'poista tentti' -napin painaminen (Admin-näkymä)
     const handleDeleteExamAdmin = (event, tenttiIndex) => {
        console.log("poista tentti indeksistä " + tenttiIndex)
        let nykyinenData = JSON.parse(JSON.stringify(tentit))
        nykyinenData.splice(tenttiIndex, 1)
        setTentit(nykyinenData);
        setValittuTentti(null)
    }

    // ADMIN: tentin nimen tekstin muuttaminen (Admin-näkymä)
    const handleExamNameChangeAdmin = (event, tenttiIndex) => {      
        let nykyinenData = JSON.parse(JSON.stringify(tentit))
        nykyinenData[tenttiIndex].nimi = event.target.value
        setTentit(nykyinenData);
    }
    

    // Kirjaudu sisään
    const handleKirjauduSisaan = (kayttajatunnus, salasana, onkoAdmin) => {
        
        if(kayttajatunnus !== "" && salasana !== "") {
            setKayttaja( kayttajatunnus )
            if (onkoAdmin) {
                setOnkoAdmin(true)
            } else {
                setOnkoAdmin(false)
            }
        } else {
            setKayttaja(null)
            setOnkoAdmin(false)
        }
        console.log('kayttajatunnus:' + kayttajatunnus + ", salasana: " + salasana + ", onko Admin: " + onkoAdmin)  
    }

    // Kirjaudu ulos
    const handleKirjauduUlos = (kayttajatunnus) => {
        console.log('kirjaudu ulos kayttajatunnus:' + kayttajatunnus) 
        setValittuTentti(null)
        setNaytaVastaukset(false)    
        setKayttaja(null)
        setOnkoAdmin(false)
    }

    // tentit-napin painaminen AppBarissa
    const handleNaytaTentit = () => {
        console.log("Näytä tentit painettu.")
        setValittuTentti(null)
        setNaytaVastaukset(false)
    }

    // useEffect käynnistyy, kun tentit-taulukko päivittyy
    useEffect( () => {

        // tallenna localStorageen vain yksi JSON-objekti(merkkijonona), joka päivittyy tarpeen mukaan
        // tentit on taulukko-olio, jota setItem ei osaa tallettaa localStorageen.
        // Siksi käytettävä JSON.stringify() -funktiota, joka muuntaa objektin ensin string-muotoon:
        if (!komponenttiAlustettu) {
            if (!localStorage.getItem("tenttiappTentit")) {
                localStorage.setItem("tenttiappTentit",JSON.stringify(tentit))
            } else {
                setTentit(JSON.parse(localStorage.getItem("tenttiappTentit")))
            }
            setKomponenttiAlustettu(true);
        }
        else {
            localStorage.setItem("tenttiappTentit", JSON.stringify(tentit));
        }     

    },[tentit]);


    return (
        <div>
            <div>
                <ButtonAppBar   kayttaja={kayttaja}
                                onkoAdmin={onkoAdmin}
                                handleKirjauduSisaan={handleKirjauduSisaan}
                                handleKirjauduUlos={handleKirjauduUlos}
                                handleNaytaTentit={handleNaytaTentit}
                                handleAddNewExamAdmin={handleAddNewExamAdmin}
                ></ButtonAppBar>
            </div>
            <div>
                {
                    kayttaja === null && <LogIn handleKirjauduSisaan={handleKirjauduSisaan}></LogIn>
                }
            </div>
            <div>{kayttaja !== null &&
                tentit.map((tentti, i) => <Button key={i.toString()} variant="text" color="primary"
                    onClick={() =>  {setValittuTentti(i)
                                    setNaytaVastaukset(false)}
                            }>{ tentti.nimi!=="" ? tentti.nimi : "[Nimetön tentti]"}</Button>
                )}
            </div>
            <div>
                {valittuTentti !== null && onkoAdmin === false && tentit[valittuTentti].kysymykset.map( (kysymys, kysymysIndex) =>
                    <Kysymys key={kysymysIndex}
                            kysymysIndex={kysymysIndex} 
                            {...kysymys} 
                            tenttiIndex={valittuTentti}
                            action={handleCheckboxChange}
                            naytaVastaukset={naytaVastaukset} />)}
            </div>
         
            <Box>               
                {valittuTentti !== null && onkoAdmin === true &&
                <DialogAlertExamDelete  handleDeleteExamAdmin={handleDeleteExamAdmin}
                                        tenttiIndex={valittuTentti}
                />
                }
            </Box>
            
            <Box>
                {valittuTentti !== null && onkoAdmin === true &&
                <TextField
                required 
                id={valittuTentti + tentit[valittuTentti].nimi}
                label={"Muokkaa tentin nimeä"}
                //defaultValue={""}
                value={tentit[valittuTentti].nimi}
                className={classes.textField}
                helperText=""
                margin="normal"
                variant="filled"
                onChange={(event) => { document.getElementById(valittuTentti + tentit[valittuTentti].nimi).value =                        
                           handleExamNameChangeAdmin(event, valittuTentti) }
                         }
                /> }
            </Box>
            
            <div>
                {valittuTentti !== null && onkoAdmin === true && tentit[valittuTentti].kysymykset.map( (kysymys, kysymysIndex) =>
                    <Admin  key={kysymysIndex.toString()}
                            kysymysIndex={kysymysIndex} 
                            {...kysymys} 
                            tenttiIndex={valittuTentti}
                            tenttiNimi={tentit[valittuTentti].nimi}
                            handleQuestionChangeAdmin={handleQuestionChangeAdmin}
                            handleCheckboxChangeAdmin={handleCheckboxChangeAdmin}
                            handleAnswerChangeAdmin={handleAnswerChangeAdmin}
                            handleQuestionCategoryChangeAdmin={handleQuestionCategoryChangeAdmin}
                            handleAddNewAnswerAdmin={handleAddNewAnswerAdmin}
                            handleDeleteAnswerAdmin={handleDeleteAnswerAdmin}
                            handleDeleteQuestionAdmin={handleDeleteQuestionAdmin}
                            handleAddNewQuestionAdmin={handleAddNewQuestionAdmin}                           
                            handleExamNameChangeAdmin={handleExamNameChangeAdmin}                  
                    />)}
            </div>
           
            <Box>
                {valittuTentti !== null && onkoAdmin === true &&
                <TextField        
                    className={classes.textField}          
                    disabled 
                    style={{ marginLeft: 10, marginTop: 10, fontSize: 30 }}
                    id="lisaa_kysymys_loppuun-standard-disabled" 
                    label={tentit[valittuTentti].nimi}
                    defaultValue={"Lisää uusi kysymys"}
                    //defaultValue={"Lisää uusi kysymys numero " + (tentit[valittuTentti].kysymykset.length +1)}
                    variant="standard"
                />              
                }
            
            
                {valittuTentti !== null && onkoAdmin === true &&
                    <AddCircleOutlineRoundedIcon 
                    style={{ color: green[500], marginLeft: 10, marginTop: 20, fontSize: 30 }}
                    onClick={(event) => handleAddNewQuestionAdmin(event, valittuTentti, tentit[valittuTentti].kysymykset.length +1) }
                />
                }
            </Box>
            
            <div>
                {valittuTentti !== null && onkoAdmin === false && naytaVastaukset === false && <Button variant="contained" color="primary"
                    onClick={() => setNaytaVastaukset(true) }>Näytä vastaukset</Button>}
            </div>
            <div>
                {valittuTentti !== null && onkoAdmin === false && naytaVastaukset === true && <Button variant="contained" color="primary"
                    onClick={() => setNaytaVastaukset(false) }>Piilota vastaukset</Button>}
            </div>

            <div>
                {valittuTentti !== null && naytaVastaukset === true &&
                <Stats data={tentit[valittuTentti].kysymykset}></Stats>}
            </div>
            
        </div>
    );
}

export default App;