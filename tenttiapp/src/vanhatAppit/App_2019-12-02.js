import React, { useState, useEffect } from 'react';
//import logo from './logo.svg';
import './App.css';
import { Box, Button, Checkbox, FormLabel } from '@material-ui/core';

const Kysymys = (props) => {
    return (
        <div>
            <h2>{props.kysymys}</h2>
            {props.vastaukset.map( (vastaus, i) => (
                <Box>
                    <FormLabel>
                        <Checkbox checked={vastaus.valittu} onChange={(event) => props.action(event, props.tenttiIndex, props.kysymysIndex, i)}>
                        </Checkbox>{vastaus.vastaus}
                    </FormLabel>
                </Box>
            ))}
        </div>
    );
}

// BURGER CLICKERISTÄ esimerkkejä:
// let coupons = localStorage.getItem("coupons");
// coupons = coupons ? JSON.parse(coupons) : []; // muunnetaan JSON string -> objektiksi, jos löytyy localStoragesta

// localStorage.setItem("coupons", JSON.stringify(coupons) );

// let clicks = parseInt(localStorage.getItem("clicks")); // muunnettava int:ksi, koska localStoragessa merkkinä
// localStorage.setItem("clicks", clicks);

/*

// TENTTI / KYSYMYKSET / yms. rakenne:

{   nimi: "Luonnontieteen koe",
            kysymykset: [   {   kysymys: "Onko kuu juustoa?", 
                                vastaukset:[{vastaus: "kyllä", valittu: true},
                                            {vastaus: "ei", valittu: false} ]},
                            {   kysymys: "Onko kissa eläin?", vastaukset:[ "ei", "ehkä","voi olla", "ei ehdi vastaamaan" ]} 
                        ]},
        {   nimi: "Matikan välikoe",
            kysymykset: [   {kysymys: "Onko kolmio monitahokas?",vastaukset:[]},
                            {kysymys: "Onko pakko, jos ei halua?",vastaukset:[]} 
                        ]}

*/

const App = () => {

    const [tentit, setTentit] = useState([
        {   nimi: "Luonnontieteen koe",
            kysymykset: [   {   kysymys: "Onko kuu juustoa?", 
                                vastaukset:[{vastaus: "kyllä", valittu: true},
                                            {vastaus: "ei", valittu: false} ]}
                        ]}
    ])

    const [valittuTentti, setValittuTentti] = useState(null)

    const handleCheckboxChange = (event, tenttiIndex, kysymysIndex, vastausIndex) => {      
        let nykyinenData = JSON.parse(JSON.stringify(tentit))
        nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[vastausIndex].valittu = event.target.checked
        setTentit(nykyinenData);
        //localStorage.setItem("eka", nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[0].valittu );
        //localStorage.setItem("toka", nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[1].valittu );
    }

    useEffect( () => {
        let nykyinenData = JSON.parse(JSON.stringify(tentit))
        if (!localStorage.getItem("eka")) {
            localStorage.setItem("eka", tentit[0].kysymykset[0].vastaukset[0].valittu)
        } else {
            nykyinenData[0].kysymykset[0].vastaukset[0].valittu = JSON.parse(localStorage.getItem("eka")) // TAI: .valittu = localStorage.getItem("eka")==="true"                                       
        }
        if (!localStorage.getItem("toka")) {
            localStorage.setItem("toka", tentit[0].kysymykset[0].vastaukset[1].valittu)
        } else {
            nykyinenData[0].kysymykset[0].vastaukset[1].valittu = JSON.parse(localStorage.getItem("toka"))
        }     
        // let nykyinenData = JSON.parse(JSON.stringify(tentit))
        // nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[0].valittu = eka
        // nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[1].valittu = toka
        setTentit(nykyinenData); 

    }, []);  // kun sovellus käynnistyy (tyhjät hakasulkeet)

    // tää pitää olla ekan useEffectin alla: päivittyy, kun tentit-taulukko päivittyy
    useEffect( () => {

        localStorage.setItem("eka", tentit[0].kysymykset[0].vastaukset[0].valittu );
        localStorage.setItem("toka", tentit[0].kysymykset[0].vastaukset[1].valittu );

        // tallenna localStorageen vain yksi JSON-objekti(merkkijonona), joka päivittyy tarpeen mukaan

    },[tentit]);


    return (
        <div>
            <div>{
                tentit.map((tentti, i) => <Button variant="contained" color="primary"
                    onClick={() => setValittuTentti(i)}>{tentti.nimi}</Button>
                )}
            </div>
            <div>
                {valittuTentti !== null && tentit[valittuTentti].kysymykset.map( (kysymys, kysymysIndex) => <Kysymys kysymysIndex={kysymysIndex} {...kysymys} tenttiIndex={valittuTentti} action={handleCheckboxChange} />)}
            </div>
        </div>
    );
}

export default App;