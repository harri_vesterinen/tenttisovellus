import React, { useState, useEffect } from 'react';
//import logo from './logo.svg';
import kuvaPepsu from './images/Pepsu_1.jpg';
import './App.css';
import { Box, Button, Checkbox, FormLabel } from '@material-ui/core';

const Kysymys = (props) => {

    const [kaikkiOikein, setKaikkiOikein] = useState(true)

    useEffect( () => {
        
        setKaikkiOikein(true)
        props.vastaukset.map( vastaus => {
            if (vastaus.valittu !== vastaus.oikeavastaus) {
                setKaikkiOikein(false)
            }
            return vastaus
        } )

    }, [props.naytaVastaukset] )

    return (
        <div>          
            <div>
                <h2>
                    {props.kysymys}
                    {props.naytaVastaukset === true && kaikkiOikein === true &&
                        <img src={kuvaPepsu} width="20" height="20" alt="Oikein!" />
                    }
                </h2>
            </div>          
            {props.vastaukset.map( (vastaus, i) => (
                <Box>
                    <FormLabel>
                        <Checkbox checked={vastaus.valittu} onChange={(event) => props.action(event, props.tenttiIndex, props.kysymysIndex, i)}>
                        </Checkbox>
                        {props.naytaVastaukset === true &&
                        <Checkbox checked={vastaus.oikeavastaus}></Checkbox>
                        }
                        {vastaus.vastaus}

                    </FormLabel>
                </Box>
            ))}
        </div>
    );
}

/*

// TENTTI / KYSYMYKSET / yms. rakenne:

{   nimi: "Luonnontieteen koe",
            kysymykset: [   {   kysymys: "Onko kuu juustoa?", 
                                vastaukset:[{vastaus: "kyllä", valittu: true},
                                            {vastaus: "ei", valittu: false} ]},
                            {   kysymys: "Onko kissa eläin?", vastaukset:[ "ei", "ehkä","voi olla", "ei ehdi vastaamaan" ]} 
                        ]},
        {   nimi: "Matikan välikoe",
            kysymykset: [   {kysymys: "Onko kolmio monitahokas?",vastaukset:[]},
                            {kysymys: "Onko pakko, jos ei halua?",vastaukset:[]} 
                        ]}

*/

const App = () => {

    const [tentit, setTentit] = useState([
        {   nimi: "Luonnontieteen koe",
            kysymykset: [   {kysymys: "Onko kuu juustoa?", 
                             vastaukset:[{vastaus: "kyllä", valittu: false, oikeavastaus: true},
                                         {vastaus: "ei", valittu: false, oikeavastaus: false}]},
                            {kysymys: "Mistä käytetään lyhennettä Fe?", 
                             vastaukset:[{vastaus: "Fenkoli", valittu: false, oikeavastaus: false},
                                         {vastaus: "Rauta", valittu: false, oikeavastaus: true}]}                        
                        ]}
    ])

    const [valittuTentti, setValittuTentti] = useState(null)
    const [naytaVastaukset, setNaytaVastaukset] = useState(false)

    const handleCheckboxChange = (event, tenttiIndex, kysymysIndex, vastausIndex) => {      
        let nykyinenData = JSON.parse(JSON.stringify(tentit))
        nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[vastausIndex].valittu = event.target.checked
        setTentit(nykyinenData);
        //localStorage.setItem("eka", nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[0].valittu );
        //localStorage.setItem("toka", nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[1].valittu );
    }

    // useEffect käynnistyy, kun sovellus avataan:
    useEffect( () => {

        // haetaan localStoragesta tentit / kysymykset / vastaukset: 
        let tentitLocalStorage = localStorage.getItem("tenttiappTentit");
        // muunnetaan tentitLocalStorage JSON-string -> objektiksi, jos löytyy localStoragesta
        // jos ei löydy localstoragesta -> käytetään sovelluksen tentit-objektia
        let nykyinenData = tentitLocalStorage ? JSON.parse(tentitLocalStorage) : JSON.parse(JSON.stringify(tentit));
        setTentit(nykyinenData); 

        /* VANHA VERSIO
        let nykyinenData = JSON.parse(JSON.stringify(tentit))
        if (!localStorage.getItem("eka")) {
            localStorage.setItem("eka", tentit[0].kysymykset[0].vastaukset[0].valittu)
        } else {
            nykyinenData[0].kysymykset[0].vastaukset[0].valittu = JSON.parse(localStorage.getItem("eka")) // TAI: .valittu = localStorage.getItem("eka")==="true"                                       
        }
        if (!localStorage.getItem("toka")) {
            localStorage.setItem("toka", tentit[0].kysymykset[0].vastaukset[1].valittu)
        } else {
            nykyinenData[0].kysymykset[0].vastaukset[1].valittu = JSON.parse(localStorage.getItem("toka"))
        }     
        // let nykyinenData = JSON.parse(JSON.stringify(tentit))
        // nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[0].valittu = eka
        // nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[1].valittu = toka
        
        setTentit(nykyinenData); 
        */

    }, []);  // kun sovellus käynnistyy (tyhjät hakasulkeet)

    // useEffect käynnistyy, kun tentit-taulukko päivittyy
    useEffect( () => {

        // tallenna localStorageen vain yksi JSON-objekti(merkkijonona), joka päivittyy tarpeen mukaan
        // tentit on taulukko-olio, jota setItem ei osaa tallettaa localStorageen.
        // Siksi käytettävä JSON.stringify() -funktiota, joka muuntaa objektin ensin string-muotoon:
        localStorage.setItem("tenttiappTentit", JSON.stringify(tentit) );

        /* VANHA VERSIO
        localStorage.setItem("eka", tentit[0].kysymykset[0].vastaukset[0].valittu );
        localStorage.setItem("toka", tentit[0].kysymykset[0].vastaukset[1].valittu );
        */       

    },[tentit]);


    return (
        <div>
            <div>{
                tentit.map((tentti, i) => <Button variant="contained" color="primary"
                    onClick={() => setValittuTentti(i)}>{tentti.nimi}</Button>
                )}
            </div>
            <div>
                {valittuTentti !== null && tentit[valittuTentti].kysymykset.map( (kysymys, kysymysIndex) =>
                    <Kysymys kysymysIndex={kysymysIndex} 
                            {...kysymys} 
                            tenttiIndex={valittuTentti}
                            action={handleCheckboxChange}
                            naytaVastaukset={naytaVastaukset} />)}
            </div>
            <div>
                {valittuTentti !== null && naytaVastaukset === false && <Button variant="contained" color="primary"
                    onClick={() => setNaytaVastaukset(true) }>Näytä vastaukset</Button>}
            </div>
            <div>
                {valittuTentti !== null && naytaVastaukset === true && <Button variant="contained" color="primary"
                    onClick={() => setNaytaVastaukset(false) }>Piilota vastaukset</Button>}
            </div>
        </div>
    );
}

export default App;