import React, { useState, useEffect, useContext } from 'react'; // useReducer poistettu
import kuvaPepsu from './images/Pepsu_1.jpg';
//import testdata from './testdata'; siirretty Context.js:ään
import './App.css';

import { Box, Button, Checkbox, FormLabel } from '@material-ui/core'; // Container poistettu
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/core/styles'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { green, blue } from '@material-ui/core/colors'; // red poistettu
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import AddCircleOutlineRoundedIcon from '@material-ui/icons/AddCircleOutlineRounded';
//import DeleteForeverRoundedIcon from '@material-ui/icons/DeleteForeverRounded';
//import { maxWidth, minWidth } from '@material-ui/system';
import ButtonAppBar from './components/AppBar/ButtonAppBar'

import LogIn from './components/LogIn/LogIn'
import Stats from './components/Stats/Stats'
import Admin from './components/Admin/Admin'
import DialogAlertExamDelete from './components/Dialogs/DialogAlertExamDelete'
import {TenttiContext} from './Context'
import TenttiSort from './components/TenttiSort/TenttiSort'

const axios = require('axios');

// ********************************

//const alkutila = testdata; // siirretty, ks. Context.js

// **************  KYSYMYS  ***************
const Kysymys = (props) => {

    //const [tentit, dispatch] = useReducer(reducer, alkutila) // reducer-funktio määritetty App.js alussa
    const [kaikkiOikein, setKaikkiOikein] = useState(true)

    //********* */
    const useStyles = makeStyles(theme => ({
        container: {
          display: 'flex',
          flexWrap: 'wrap',
          marginLeft: theme.spacing(2),
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 800,
        },
        formControl: {
            margin: theme.spacing(2),
            marginLeft: theme.spacing(1),
            minWidth: 120,
        },
        
      }));
    const classes = useStyles();
    /************ */

    // vihreä CheckBox
    const GreenCheckbox = withStyles({
        root: {
          color: green[400],
          '&$checked': {
            color: green[600],
          },
        },
        checked: {},
    })(props => <Checkbox color="default" {...props} />);

    // sininen CheckBox
    const BlueCheckbox = withStyles({
        root: {
          color: blue[400],
          '&$checked': {
            color: blue[600],
          },
        },
        checked: {},
    })(props => <Checkbox color="default" {...props} />);

    // kun painettu 'näytä vastaukset'-nappia App-komponentissa:
    useEffect( () => {
        
        setKaikkiOikein(true)
        props.vastaukset.map( vastaus => {
            if (vastaus.valittu !== vastaus.oikeavastaus) {
                setKaikkiOikein(false)
            }
            return vastaus
        } )

    }, [props.naytaVastaukset] )

    // Kysymys-piirto
    return (
    
        <Paper>                    
            <Box>                          
                
                <TextField
                    required
                    id={props.tenttiIndex + props.kysymysIndex + props.kysymys}
                    label={props.tenttiNimi}
                    //defaultValue={props.kysymys}
                    value={ (props.kysymysIndex + 1) + ". " + (props.kysymys) }
                    className={classes.textField}
                    helperText={props.tenttiNimi}
                    margin="none"
                    variant="filled"                 
                />

                {props.naytaVastaukset === true && kaikkiOikein === true &&
                    <img src={kuvaPepsu} width="55" height="55" alt="Oikein!" />}                   
            </Box>
                       
            {props.vastaukset.map( (vastaus, i) => (
                <Box key={i.toString()}>
                    <FormLabel>
                        <GreenCheckbox checked={vastaus.valittu} 
                            onChange={props.naytaVastaukset === false ? 
                                        (event) => props.dispatch(
                                            {nimi: "checkboxChangeUser",
                                            arvo: event.target.checked,
                                            tapahtumatyyppi: 'muuta',
                                            tenttiIndex: props.tenttiIndex,
                                            kysymysIndex: props.kysymysIndex,
                                            vastausIndex: i }
                                            ) : undefined }>
                        </GreenCheckbox>
                    </FormLabel>
                    {props.naytaVastaukset === true &&
                    <FormControlLabel
                            control={
                                <BlueCheckbox checked={vastaus.oikeavastaus} />
                             }
                            label=""
                    />
                    }
                    {vastaus.vastaus}
                </Box>
                ))
            }

        </Paper>
    );
}

//************  TENTTIAPP  ************************** */
const TenttiApp = () => {

    const { tentit, dispatch } = useContext(TenttiContext);
    //VANHA: const [tentit, setTentit] = useState(testdata) // siirretty Context.js: const alkutila = testdata;
    const [kayttaja, setKayttaja] = useState(null)
    const [onkoAdmin, setOnkoAdmin] = useState(false)
    const [valittuTentti, setValittuTentti] = useState(null)
    const [naytaVastaukset, setNaytaVastaukset] = useState(false)
    const [komponenttiAlustettu, setKomponenttiAlustettu] = useState(false)
    const [aakkosjärjestys, setAakkosjärjestys] = useState(true)

    // 'express-palvelin'-hakemiston palvelin: portissa 3000
    //const [url, setUrl] = useState("http://localhost:3000/");

    //********* */
    const useStyles = makeStyles(theme => ({
        container: {
          display: 'flex',
          flexWrap: 'wrap',
          marginLeft: theme.spacing(2),
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 700,
        },
        formControl: {
            margin: theme.spacing(2),
            marginLeft: theme.spacing(1),
            minWidth: 120,
        },
        
      }));
    const classes = useStyles();
    /************ */

    // Kirjaudu sisään
    const handleKirjauduSisaan = (kayttajatunnus, salasana, onkoAdmin) => {
        
        if(kayttajatunnus !== "" && salasana !== "") {
            setKayttaja( kayttajatunnus )
            if (onkoAdmin) {
                setOnkoAdmin(true)
            } else {
                setOnkoAdmin(false)
            }
        } else {
            setKayttaja(null)
            setOnkoAdmin(false)
        }
        console.log('kayttajatunnus:' + kayttajatunnus + ", salasana: " + salasana + ", onko Admin: " + onkoAdmin)  
    }

    // Kirjaudu ulos
    const handleKirjauduUlos = (kayttajatunnus) => {
        console.log('kirjaudu ulos kayttajatunnus:' + kayttajatunnus) 
        setValittuTentti(null)
        setNaytaVastaukset(false)    
        setKayttaja(null)
        setOnkoAdmin(false)
    }

    // tentit-napin painaminen AppBarissa
    const handleNaytaTentit = () => {
        console.log("Näytä tentit painettu.")
        setValittuTentti(null)
        setNaytaVastaukset(false)
    }


// AXIOS:in KÄYTTÖ
    // useEffect käynnistyy, kun tentit-taulukko päivittyy
    useEffect( () => {

        // tallenna localStorageen vain yksi JSON-objekti(merkkijonona), joka päivittyy tarpeen mukaan
        // tentit on taulukko-olio, jota setItem ei osaa tallettaa localStorageen.
        // Siksi käytettävä JSON.stringify() -funktiota, joka muuntaa objektin ensin string-muotoon:
        if (!komponenttiAlustettu) {
            if (!localStorage.getItem("tenttiappTentit")) {
                localStorage.setItem("tenttiappTentit",JSON.stringify(tentit))
            } else {

/*
                axios.post(url + '/haeTentit')
                  .then(function (response) {
                    console.log(response + ' KUKKUU');
                  })
                  .catch(function (error) {
                    console.log(error);
                  });
*/



                dispatch({tapahtumatyyppi: 'initialize', data: JSON.parse(localStorage.getItem("tenttiappTentit"))})
                //vanha: setTentit(JSON.parse(localStorage.getItem("tenttiappTentit")))
            }
            setKomponenttiAlustettu(true);
        }
        else {
            localStorage.setItem("tenttiappTentit", JSON.stringify(tentit));
        }     

    },[tentit]);


// ALKUPERÄINEN LOCALSTORAGEN KAUTTA TOIMIVA ALLA:
/*
    // useEffect käynnistyy, kun tentit-taulukko päivittyy
    useEffect( () => {

        // tallenna localStorageen vain yksi JSON-objekti(merkkijonona), joka päivittyy tarpeen mukaan
        // tentit on taulukko-olio, jota setItem ei osaa tallettaa localStorageen.
        // Siksi käytettävä JSON.stringify() -funktiota, joka muuntaa objektin ensin string-muotoon:
        if (!komponenttiAlustettu) {
            if (!localStorage.getItem("tenttiappTentit")) {
                localStorage.setItem("tenttiappTentit",JSON.stringify(tentit))
            } else {
                dispatch({tapahtumatyyppi: 'initialize', data: JSON.parse(localStorage.getItem("tenttiappTentit"))})
                //vanha: setTentit(JSON.parse(localStorage.getItem("tenttiappTentit")))
            }
            setKomponenttiAlustettu(true);
        }
        else {
            localStorage.setItem("tenttiappTentit", JSON.stringify(tentit));
        }     

    },[tentit]);

*/

    // TenttiApp-piirto
    return (
        <div>
            <div>
                <ButtonAppBar   kayttaja={kayttaja}
                                onkoAdmin={onkoAdmin}
                                handleKirjauduSisaan={handleKirjauduSisaan}
                                handleKirjauduUlos={handleKirjauduUlos}
                                handleNaytaTentit={handleNaytaTentit}
                                //VANHA: handleAddNewExamAdmin={handleAddNewExamAdmin}
                                dispatch={dispatch}
                                setValittuTentti={setValittuTentti}
                                tenttiLength={tentit.length}        
                ></ButtonAppBar>
            </div>
            <div>
                {
                    kayttaja === null && <LogIn handleKirjauduSisaan={handleKirjauduSisaan}></LogIn>
                }
            </div>
            <div>
                {kayttaja !== null &&
                // tenttien sorttaustoiminto: 
                <TenttiSort aakkosjärjestys={aakkosjärjestys}
                            setAakkosjärjestys={setAakkosjärjestys}
                            setValittuTentti={setValittuTentti}
                            kayttaja={kayttaja}
                            onkoAdmin={onkoAdmin}>
                </TenttiSort>
                }

                {kayttaja !== null &&
                // tenttien näyttäminen
                tentit.map((tentti, i) => <Button key={i.toString()} variant="text" color="primary"
                    onClick={() =>  {setValittuTentti(i)
                                    setNaytaVastaukset(false)}
                            }>{ tentti.nimi!=="" ? tentti.nimi : "[Nimetön tentti]"}</Button>
                )}
            </div>
            <div>
                {valittuTentti !== null && onkoAdmin === false && tentit[valittuTentti].kysymykset.map( (kysymys, kysymysIndex) =>
                    <Kysymys key={kysymysIndex}
                            kysymysIndex={kysymysIndex} 
                            {...kysymys} 
                            tenttiIndex={valittuTentti}
                            dispatch={dispatch}
                            //VANHA: action={handleCheckboxChange}
                            naytaVastaukset={naytaVastaukset} />)}
            </div>
         
            <Box>               
                {valittuTentti !== null && onkoAdmin === true &&
                <DialogAlertExamDelete  //VANHA: handleDeleteExamAdmin={handleDeleteExamAdmin}
                                        dispatch={dispatch}
                                        tenttiIndex={valittuTentti}
                                        setValittuTentti={setValittuTentti}
                />
                }
            </Box>
            
            <Box>
                {valittuTentti !== null && onkoAdmin === true &&
                <TextField
                    required 
                    id={valittuTentti + tentit[valittuTentti].nimi}
                    label={"Muokkaa tentin nimeä"}
                    //defaultValue={""}
                    value={tentit[valittuTentti].nimi}
                    className={classes.textField}
                    helperText=""
                    margin="normal"
                    variant="filled"
                    // VANHA: onChange={(event) => { document.getElementById(valittuTentti + tentit[valittuTentti].nimi).value =                        
                    //          handleExamNameChangeAdmin(event, valittuTentti) }}
                    onChange={(event) => { document.getElementById(valittuTentti + tentit[valittuTentti].nimi).value = 
                                dispatch(
                                    {   nimi: "examNameChangeAdmin",
                                        arvo: event.target.value,
                                        tapahtumatyyppi: 'muuta',
                                        tenttiIndex: valittuTentti,
                                        kysymysIndex: undefined,
                                        vastausIndex: undefined 
                                    }
                                ) }
                            }
                /> }
            </Box>
            
            <div>
                {valittuTentti !== null && onkoAdmin === true && tentit[valittuTentti].kysymykset.map( (kysymys, kysymysIndex) =>
                    <Admin  key={kysymysIndex.toString()}
                            kysymysIndex={kysymysIndex} 
                            {...kysymys} 
                            tenttiIndex={valittuTentti}
                            tenttiNimi={tentit[valittuTentti].nimi}
                            dispatch={dispatch}
                            // VANHAT TILAMUUTOSKÄSITTELIJÄT:
                            //handleQuestionChangeAdmin={handleQuestionChangeAdmin}
                            //handleCheckboxChangeAdmin={handleCheckboxChangeAdmin}
                            //handleAnswerChangeAdmin={handleAnswerChangeAdmin}
                            //handleQuestionCategoryChangeAdmin={handleQuestionCategoryChangeAdmin}
                            //handleAddNewAnswerAdmin={handleAddNewAnswerAdmin}
                            //handleDeleteAnswerAdmin={handleDeleteAnswerAdmin}
                            //handleDeleteQuestionAdmin={handleDeleteQuestionAdmin}
                            //handleAddNewQuestionAdmin={handleAddNewQuestionAdmin}                           
                            //handleExamNameChangeAdmin={handleExamNameChangeAdmin}                                                                      
                    />)}
            </div>
           
            <Box>
                {valittuTentti !== null && onkoAdmin === true &&
                <TextField        
                    className={classes.textField}          
                    disabled 
                    style={{ marginLeft: 10, marginTop: 10, fontSize: 30 }}
                    id="lisaa_kysymys_loppuun-standard-disabled" 
                    label={tentit[valittuTentti].nimi}
                    defaultValue={"Lisää uusi kysymys"}
                    variant="standard"
                />              
                }
            
            
                {valittuTentti !== null && onkoAdmin === true &&
                    <AddCircleOutlineRoundedIcon 
                    style={{ color: green[500], marginLeft: 10, marginTop: 20, fontSize: 30 }}
                    //VANHA: onClick={(event) => handleAddNewQuestionAdmin(event, valittuTentti, tentit[valittuTentti].kysymykset.length +1) }
                    onClick={() => 
                        dispatch(
                            {nimi: "questionAddAdmin",
                            arvo: undefined,
                            tapahtumatyyppi: 'lisää',
                            tenttiIndex: valittuTentti,
                            kysymysIndex: tentit[valittuTentti].kysymykset.length +1,
                            vastausIndex: undefined }
                        )
                    }
                />
                }
            </Box>
            
            <div>
                {valittuTentti !== null && onkoAdmin === false && naytaVastaukset === false && <Button variant="contained" color="primary"
                    onClick={() => setNaytaVastaukset(true) }>Näytä vastaukset</Button>}
            </div>
            <div>
                {valittuTentti !== null && onkoAdmin === false && naytaVastaukset === true && <Button variant="contained" color="primary"
                    onClick={() => setNaytaVastaukset(false) }>Piilota vastaukset</Button>}
            </div>

            <div>
                {valittuTentti !== null && naytaVastaukset === true &&
                <Stats data={tentit[valittuTentti].kysymykset}></Stats>}
            </div>
            
        </div>
    );
}

export default TenttiApp;