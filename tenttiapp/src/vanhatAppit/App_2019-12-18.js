import React, { useState, useEffect, useReducer } from 'react';
import kuvaPepsu from './images/Pepsu_1.jpg';
import testdata from './testdata';
import './App.css';

import { Box, Button, Checkbox, FormLabel } from '@material-ui/core'; // Container poistettu
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/core/styles'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { green, blue } from '@material-ui/core/colors'; // red poistettu
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import AddCircleOutlineRoundedIcon from '@material-ui/icons/AddCircleOutlineRounded';
//import DeleteForeverRoundedIcon from '@material-ui/icons/DeleteForeverRounded';
//import { maxWidth, minWidth } from '@material-ui/system';
import ButtonAppBar from './components/AppBar/ButtonAppBar'

import LogIn from './components/LogIn/LogIn'
import Stats from './components/Stats/Stats'
import Admin from './components/Admin/Admin'
import DialogAlertExamDelete from './components/Dialogs/DialogAlertExamDelete'
//import { maxWidth } from '@material-ui/system';

// ********************************

// ********** geneerinen tapahtumankäsittelijä: ************
//toiminto ={nimi: a, arvo: b, tapahtumatyyppi: 'poista'}
//toiminto ={nimi: a, arvo: b, tapahtumatyyppi: 'lisää'}
//toiminto ={nimi: a, arvo: b, tapahtumatyyppi: 'muuta'}

const alkutila = testdata;

// tila vastaa 'tentit' -useState-muuttujan arvoa
const reducer = (tila, toiminto) => {

    // MUUTA-TOIMINNOT
    if (toiminto.tapahtumatyyppi === 'muuta') {
        let nimi = toiminto.nimi; // mikä toiminto kyseessä
        let arvo = toiminto.arvo; // uusi toiminnon kohteen tila
        let tenttiIndex = toiminto.tenttiIndex;
        let kysymysIndex = toiminto.kysymysIndex;
        let vastausIndex = toiminto.vastausIndex; // HUOM: voidaan käsitellä spreadillä, FixMe!
        let nykyinenData = JSON.parse(JSON.stringify(tila)) // TAI: let nykyinenTila = [...tila]
        // käyttäjä muuttaa vastausruksia (checkbox):
        if (nimi==="checkboxChangeUser") {
            nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[vastausIndex].valittu = arvo;
        // admin muuttaa oikeaa vastausruksia (checkbox):
        } else if (nimi==="checkboxChangeAdmin") {
            nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[vastausIndex].oikeavastaus = arvo;
        // admin muuttaa kysymyksen sisältöä (text field):
        } else if (nimi=== "questionChangeAdmin") {
            nykyinenData[tenttiIndex].kysymykset[kysymysIndex].kysymys = arvo;
        // admin muuttaa kysymyksen aihepiiriä (valintalista):
        } else if (nimi==="questionCategoryChangeAdmin") {
            nykyinenData[tenttiIndex].kysymykset[kysymysIndex].aihepiiri = arvo;
        // admin muuttaa vastauksen sisältöä (text field):
        } else if (nimi=== "answerChangeAdmin") {
            nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset[vastausIndex].vastaus = arvo;
        // admin muuttaa tentin nimeä (text field):
        } else if (nimi==="examNameChangeAdmin") {
            nykyinenData[tenttiIndex].nimi = arvo;
        }
        
        return nykyinenData;

    // POISTA TOIMINNOT
    } else if (toiminto.tapahtumatyyppi === 'poista') {
        let nimi = toiminto.nimi; // mikä toiminto kyseessä
        //let arvo = toiminto.arvo; // uusi toiminnon kohteen tila
        let tenttiIndex = toiminto.tenttiIndex;
        let kysymysIndex = toiminto.kysymysIndex;
        let vastausIndex = toiminto.vastausIndex; // HUOM: voidaan käsitellä spreadillä, FixMe!
        let nykyinenData = JSON.parse(JSON.stringify(tila)) // TAI: let nykyinenTila = [...tila]
        // admin poistaa vastauksen (vastauksen roskisnapit):
        if (nimi==="deleteAnswerAdmin") {
            nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset.splice(vastausIndex, 1)
        } 
        // admin poistaa kysymyksen (kysymyksen roskisnapit):
        else if (nimi==="deleteQuestionAdmin") {
            nykyinenData[tenttiIndex].kysymykset.splice(kysymysIndex, 1)
        }
        // admin poistaa tentin ('poista tentti' roskisnappi): EI KÄYTÖSSÄ NYT TÄSSÄ REDUCERISSA
        else if (nimi==="examDeleteAdmin") {
            nykyinenData.splice(tenttiIndex, 1)
            //setValittuTentti(null) -> tää jonnekin muualle, ei toimi tässä!!!
        }

        return nykyinenData;

    // LISÄÄ TOIMINNOT
    } else if (toiminto.tapahtumatyyppi=== 'lisää') {

        let nimi = toiminto.nimi; // mikä toiminto kyseessä
        //let arvo = toiminto.arvo; // uusi toiminnon kohteen tila
        let tenttiIndex = toiminto.tenttiIndex;
        let kysymysIndex = toiminto.kysymysIndex;
        let vastausIndex = toiminto.vastausIndex; // HUOM: voidaan käsitellä spreadillä, FixMe!
        let nykyinenData = JSON.parse(JSON.stringify(tila)) // TAI: let nykyinenTila = [...tila]
        // admin lisää uuden vastauksen (vastauksen '+'-napit):
        if (nimi==="answerAddAdmin") {
            // esim. {vastaus: "Maapallon ja ulkoavaruuden ilmiöitä", valittu: false, oikeavastaus: false}
            let uusiVastaus = {vastaus: "", valittu: false, oikeavastaus: false}
            if (vastausIndex !== undefined) {
                nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset.splice(vastausIndex, 0, uusiVastaus)
            } else {
                nykyinenData[tenttiIndex].kysymykset[kysymysIndex].vastaukset.push(uusiVastaus)
            }
        }
        // admin lisää uuden kysymyksen (kysymyksen '+'-napit)
        else if (nimi==="questionAddAdmin") {
            let uusiKysymys = { kysymys: "", 
                                aihepiiri: "", 
                                vastaukset: [
                                    {vastaus: "", valittu: false, oikeavastaus: false}
                            ] } 
            nykyinenData[tenttiIndex].kysymykset.splice(kysymysIndex, 0, uusiKysymys)
        }
        // admin lisää uuden tentin (AppBarin 'lisää tentti' -toimintonappi):
        else if (nimi=== "examAddAdmin") {
            let uusiTentti = {
                nimi: "",
                kysymykset: []
            }
            nykyinenData.splice(tila.length, 0, uusiTentti)
            //setValittuTentti(tila.length)
        }

        return nykyinenData;

    // ALKUDATAN asettaminen: kun komponentti ei ole vielä alustettu:
    } else if (toiminto.tapahtumatyyppi=== 'initialize') {
        return toiminto.data

    // PALAUTA MUUTOIN VIRHE:
    } else {
        throw new Error();
    }

}
//************************************ */

// **************  KYSYMYS  ***************
const Kysymys = (props) => {

    //const [tentit, dispatch] = useReducer(reducer, alkutila) // reducer-funktio määritetty App.js alussa
    const [kaikkiOikein, setKaikkiOikein] = useState(true)

    //********* */
    const useStyles = makeStyles(theme => ({
        container: {
          display: 'flex',
          flexWrap: 'wrap',
          marginLeft: theme.spacing(2),
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 800,
        },
        formControl: {
            margin: theme.spacing(2),
            marginLeft: theme.spacing(1),
            minWidth: 120,
        },
        
      }));
    const classes = useStyles();
    /************ */

    // vihreä CheckBox
    const GreenCheckbox = withStyles({
        root: {
          color: green[400],
          '&$checked': {
            color: green[600],
          },
        },
        checked: {},
    })(props => <Checkbox color="default" {...props} />);

    // sininen CheckBox
    const BlueCheckbox = withStyles({
        root: {
          color: blue[400],
          '&$checked': {
            color: blue[600],
          },
        },
        checked: {},
    })(props => <Checkbox color="default" {...props} />);

    // kun painettu 'näytä vastaukset'-nappia App-komponentissa:
    useEffect( () => {
        
        setKaikkiOikein(true)
        props.vastaukset.map( vastaus => {
            if (vastaus.valittu !== vastaus.oikeavastaus) {
                setKaikkiOikein(false)
            }
            return vastaus
        } )

    }, [props.naytaVastaukset] )

    // Kysymys-piirto
    return (
    
        <Paper>                    
            <Box>                          
                
                <TextField
                    required
                    id={props.tenttiIndex + props.kysymysIndex + props.kysymys}
                    label={props.tenttiNimi}
                    //defaultValue={props.kysymys}
                    value={ (props.kysymysIndex + 1) + ". " + (props.kysymys) }
                    className={classes.textField}
                    helperText={props.tenttiNimi}
                    margin="none"
                    variant="filled"                 
                />

                {props.naytaVastaukset === true && kaikkiOikein === true &&
                    <img src={kuvaPepsu} width="55" height="55" alt="Oikein!" />}                   
            </Box>
                       
            {props.vastaukset.map( (vastaus, i) => (
                <Box key={i.toString()}>
                    <FormLabel>
                        <GreenCheckbox checked={vastaus.valittu} 
                            onChange={props.naytaVastaukset === false ? 
                                        (event) => props.dispatch(
                                            {nimi: "checkboxChangeUser",
                                            arvo: event.target.checked,
                                            tapahtumatyyppi: 'muuta',
                                            tenttiIndex: props.tenttiIndex,
                                            kysymysIndex: props.kysymysIndex,
                                            vastausIndex: i }
                                            ) : undefined }>
                        </GreenCheckbox>
                    </FormLabel>
                    {props.naytaVastaukset === true &&
                    <FormControlLabel
                            control={
                                <BlueCheckbox checked={vastaus.oikeavastaus} />
                             }
                            label=""
                    />
                    }
                    {vastaus.vastaus}
                </Box>
                ))
            }

        </Paper>
    );
}

//************  APP  ************************** */
const App = () => {

    const [tentit, dispatch] = useReducer(reducer, alkutila) // reducer-funktio määritetty App.js alussa
    //VANHA: const [tentit, setTentit] = useState(testdata) // siirretty App.js alkuun: const alkutila = testdata;
    const [kayttaja, setKayttaja] = useState(null)
    const [onkoAdmin, setOnkoAdmin] = useState(false)
    const [valittuTentti, setValittuTentti] = useState(null)
    const [naytaVastaukset, setNaytaVastaukset] = useState(false)
    const [komponenttiAlustettu, setKomponenttiAlustettu] = useState(false)

    //********* */
    const useStyles = makeStyles(theme => ({
        container: {
          display: 'flex',
          flexWrap: 'wrap',
          marginLeft: theme.spacing(2),
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 700,
        },
        formControl: {
            margin: theme.spacing(2),
            marginLeft: theme.spacing(1),
            minWidth: 120,
        },
        
      }));
    const classes = useStyles();
    /************ */

    // Kirjaudu sisään
    const handleKirjauduSisaan = (kayttajatunnus, salasana, onkoAdmin) => {
        
        if(kayttajatunnus !== "" && salasana !== "") {
            setKayttaja( kayttajatunnus )
            if (onkoAdmin) {
                setOnkoAdmin(true)
            } else {
                setOnkoAdmin(false)
            }
        } else {
            setKayttaja(null)
            setOnkoAdmin(false)
        }
        console.log('kayttajatunnus:' + kayttajatunnus + ", salasana: " + salasana + ", onko Admin: " + onkoAdmin)  
    }

    // Kirjaudu ulos
    const handleKirjauduUlos = (kayttajatunnus) => {
        console.log('kirjaudu ulos kayttajatunnus:' + kayttajatunnus) 
        setValittuTentti(null)
        setNaytaVastaukset(false)    
        setKayttaja(null)
        setOnkoAdmin(false)
    }

    // tentit-napin painaminen AppBarissa
    const handleNaytaTentit = () => {
        console.log("Näytä tentit painettu.")
        setValittuTentti(null)
        setNaytaVastaukset(false)
    }

    // useEffect käynnistyy, kun tentit-taulukko päivittyy
    useEffect( () => {

        // tallenna localStorageen vain yksi JSON-objekti(merkkijonona), joka päivittyy tarpeen mukaan
        // tentit on taulukko-olio, jota setItem ei osaa tallettaa localStorageen.
        // Siksi käytettävä JSON.stringify() -funktiota, joka muuntaa objektin ensin string-muotoon:
        if (!komponenttiAlustettu) {
            if (!localStorage.getItem("tenttiappTentit")) {
                localStorage.setItem("tenttiappTentit",JSON.stringify(tentit))
            } else {
                dispatch({tapahtumatyyppi: 'initialize', data: JSON.parse(localStorage.getItem("tenttiappTentit"))})
                //vanha: setTentit(JSON.parse(localStorage.getItem("tenttiappTentit")))
            }
            setKomponenttiAlustettu(true);
        }
        else {
            localStorage.setItem("tenttiappTentit", JSON.stringify(tentit));
        }     

    },[tentit]);

    // App-piirto
    return (
        <div>
            <div>
                <ButtonAppBar   kayttaja={kayttaja}
                                onkoAdmin={onkoAdmin}
                                handleKirjauduSisaan={handleKirjauduSisaan}
                                handleKirjauduUlos={handleKirjauduUlos}
                                handleNaytaTentit={handleNaytaTentit}
                                //VANHA: handleAddNewExamAdmin={handleAddNewExamAdmin}
                                dispatch={dispatch}
                                setValittuTentti={setValittuTentti}
                                tenttiLength={tentit.length}        
                ></ButtonAppBar>
            </div>
            <div>
                {
                    kayttaja === null && <LogIn handleKirjauduSisaan={handleKirjauduSisaan}></LogIn>
                }
            </div>
            <div>{kayttaja !== null &&
                tentit.map((tentti, i) => <Button key={i.toString()} variant="text" color="primary"
                    onClick={() =>  {setValittuTentti(i)
                                    setNaytaVastaukset(false)}
                            }>{ tentti.nimi!=="" ? tentti.nimi : "[Nimetön tentti]"}</Button>
                )}
            </div>
            <div>
                {valittuTentti !== null && onkoAdmin === false && tentit[valittuTentti].kysymykset.map( (kysymys, kysymysIndex) =>
                    <Kysymys key={kysymysIndex}
                            kysymysIndex={kysymysIndex} 
                            {...kysymys} 
                            tenttiIndex={valittuTentti}
                            dispatch={dispatch}
                            //VANHA: action={handleCheckboxChange}
                            naytaVastaukset={naytaVastaukset} />)}
            </div>
         
            <Box>               
                {valittuTentti !== null && onkoAdmin === true &&
                <DialogAlertExamDelete  //VANHA: handleDeleteExamAdmin={handleDeleteExamAdmin}
                                        dispatch={dispatch}
                                        tenttiIndex={valittuTentti}
                                        setValittuTentti={setValittuTentti}
                />
                }
            </Box>
            
            <Box>
                {valittuTentti !== null && onkoAdmin === true &&
                <TextField
                    required 
                    id={valittuTentti + tentit[valittuTentti].nimi}
                    label={"Muokkaa tentin nimeä"}
                    //defaultValue={""}
                    value={tentit[valittuTentti].nimi}
                    className={classes.textField}
                    helperText=""
                    margin="normal"
                    variant="filled"
                    // VANHA: onChange={(event) => { document.getElementById(valittuTentti + tentit[valittuTentti].nimi).value =                        
                    //          handleExamNameChangeAdmin(event, valittuTentti) }}
                    onChange={(event) => { document.getElementById(valittuTentti + tentit[valittuTentti].nimi).value = 
                                dispatch(
                                    {   nimi: "examNameChangeAdmin",
                                        arvo: event.target.value,
                                        tapahtumatyyppi: 'muuta',
                                        tenttiIndex: valittuTentti,
                                        kysymysIndex: undefined,
                                        vastausIndex: undefined 
                                    }
                                ) }
                            }
                /> }
            </Box>
            
            <div>
                {valittuTentti !== null && onkoAdmin === true && tentit[valittuTentti].kysymykset.map( (kysymys, kysymysIndex) =>
                    <Admin  key={kysymysIndex.toString()}
                            kysymysIndex={kysymysIndex} 
                            {...kysymys} 
                            tenttiIndex={valittuTentti}
                            tenttiNimi={tentit[valittuTentti].nimi}
                            dispatch={dispatch}
                            // VANHAT TILAMUUTOSKÄSITTELIJÄT:
                            //handleQuestionChangeAdmin={handleQuestionChangeAdmin}
                            //handleCheckboxChangeAdmin={handleCheckboxChangeAdmin}
                            //handleAnswerChangeAdmin={handleAnswerChangeAdmin}
                            //handleQuestionCategoryChangeAdmin={handleQuestionCategoryChangeAdmin}
                            //handleAddNewAnswerAdmin={handleAddNewAnswerAdmin}
                            //handleDeleteAnswerAdmin={handleDeleteAnswerAdmin}
                            //handleDeleteQuestionAdmin={handleDeleteQuestionAdmin}
                            //handleAddNewQuestionAdmin={handleAddNewQuestionAdmin}                           
                            //handleExamNameChangeAdmin={handleExamNameChangeAdmin}                                                                      
                    />)}
            </div>
           
            <Box>
                {valittuTentti !== null && onkoAdmin === true &&
                <TextField        
                    className={classes.textField}          
                    disabled 
                    style={{ marginLeft: 10, marginTop: 10, fontSize: 30 }}
                    id="lisaa_kysymys_loppuun-standard-disabled" 
                    label={tentit[valittuTentti].nimi}
                    defaultValue={"Lisää uusi kysymys"}
                    variant="standard"
                />              
                }
            
            
                {valittuTentti !== null && onkoAdmin === true &&
                    <AddCircleOutlineRoundedIcon 
                    style={{ color: green[500], marginLeft: 10, marginTop: 20, fontSize: 30 }}
                    //VANHA: onClick={(event) => handleAddNewQuestionAdmin(event, valittuTentti, tentit[valittuTentti].kysymykset.length +1) }
                    onClick={() => 
                        dispatch(
                            {nimi: "questionAddAdmin",
                            arvo: undefined,
                            tapahtumatyyppi: 'lisää',
                            tenttiIndex: valittuTentti,
                            kysymysIndex: tentit[valittuTentti].kysymykset.length +1,
                            vastausIndex: undefined }
                        )
                    }
                />
                }
            </Box>
            
            <div>
                {valittuTentti !== null && onkoAdmin === false && naytaVastaukset === false && <Button variant="contained" color="primary"
                    onClick={() => setNaytaVastaukset(true) }>Näytä vastaukset</Button>}
            </div>
            <div>
                {valittuTentti !== null && onkoAdmin === false && naytaVastaukset === true && <Button variant="contained" color="primary"
                    onClick={() => setNaytaVastaukset(false) }>Piilota vastaukset</Button>}
            </div>

            <div>
                {valittuTentti !== null && naytaVastaukset === true &&
                <Stats data={tentit[valittuTentti].kysymykset}></Stats>}
            </div>
            
        </div>
    );
}

export default App;