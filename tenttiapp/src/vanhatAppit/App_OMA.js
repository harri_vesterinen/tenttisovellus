import React, {useState, useEffect} from 'react';
//import logo from './logo.svg';
import './App.css';
// omat komponentit:
import Header from '../components/Header/Header';
//import Menu from './components/Menu/Menu';
//import testdata from './testdata';

// material-ui -jutut:
//import Button from '@material-ui/core/Button';

/* 
HUOM: ks. https://material-ui.com/getting-started/installation/
  Lisätty index.html -tiedostoon ao:
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />

*/

/* SOVELLUKSEN NAPIT KUN EI OLE KIRJAUDUTTU
KIRJAUDU   REKISTERÖIDY   TIETOA SOVELLUKSESTA

SOVELLUKSEN NAPIT KUN ON KIRJAUDUTTU
TENTIT   TIETOA SOVELLUKSESTA           POISTU

*/

/*
TENTTIRAKENNE:
[{ tenttiId: id,
  tenttiNimi: nimi,
  tenttiKysymys: [{kysymys: string,
                  vastaus: [string] }]
}]


*/




function App() {


/* KOODISSA VIRHEITÄ

  // tentit haettava serveriltä lopuksi
  const [valittuTentti, setValittuTentti] = useState(testdata)
  const [tentit, setTentit] = useState(["Matikan koe", "Ohjelmoinnin perusteet"])

  function naytaTentti(valittuTentti) {
    console.log(valittuTentti)
    console.log(testdata)
  }

  return (
    <div className="App">
      <Header />
      <Menu valittuTentti={valittuTentti} naytaTentti={naytaTentti} />
      <br /> 
    </div>
    tentit.map(tentti =><Button variant="contained" color="primary">{tentti}</Button> )
  );
}

*/

return(
  <div>
  <Header />
  </div>
);
}

export default App;
