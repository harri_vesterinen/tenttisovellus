import React from 'react';

// käytä jotain google charttia piirtämiseen / ei tehty

import './Stats.css';

function Stats(props) {

  // reducer funktio:
  const reducer = (groupedData, currentItem) => {
    // haetaan index kysymyksen aihepiiri_id:n mukaan groupedData-taulukosta
    var index = groupedData.findIndex( item => item.aihepiiri_id === currentItem.aihepiiri_id );
      // haetaan index kysymyksen aihepiirin mukaan groupedData-taulukosta
      index = groupedData.findIndex( kysymys => kysymys.aihepiiri_id === currentItem.aihepiiri_id );
      let KysymysOikein = true
      // käy läpi vastaukset
      currentItem.vastaukset.map(vastaus => {
        if (vastaus.kayttajan_vastaus !== vastaus.oikein) {
          KysymysOikein = false
        }
      return vastaus
      })

      // jos index löytyy eli kysymyksen aihepiiri on jo groupedData-taulukossa
      if ( index >= 0 ) {   
        if (KysymysOikein) {
          groupedData[index].oikeinMaara = groupedData[index].oikeinMaara + 1;
        } else {
          groupedData[index].vaarinMaara = groupedData[index].vaarinMaara + 1;
        }
        groupedData[index].kysymysMaara = groupedData[index].kysymysMaara + 1;

      } else {
        // aihepiiriä ei vielä ole -> lisätään groupedDatan loppuun uusi aihepiiri ja onko vastaus oikein:
        if (KysymysOikein) {
          groupedData.push( { aihepiiri_id: currentItem.aihepiiri_id,
                              aihepiiri_nimi: currentItem.aihepiiri_nimi,
                              oikeinMaara: 1,
                              vaarinMaara: 0,
                              kysymysMaara: 1} );
        } else {
          groupedData.push( { aihepiiri_id: currentItem.aihepiiri_id,
                              aihepiiri_nimi: currentItem.aihepiiri_nimi,
                              oikeinMaara: 0,
                              vaarinMaara: 1,
                              kysymysMaara: 1 } );
        }
      }
    // palautetaan groupedData-taulukko:
    return groupedData;
  }

  // tulokset
  function naytaTulokset(groupedData) {   
    console.log("Näytätulokset")
    var newline = String.fromCharCode(13, 10);
    var tulos = "";
    tulos = groupedData.map( (aihepiiri_id) => {  
          let tulos2 = ""
          tulos2 += "Aihepiiri: " + aihepiiri_id.aihepiiri_nimi
          tulos2 += newline
          tulos2 += "- oikein kpl: " + aihepiiri_id.oikeinMaara + "/" + aihepiiri_id.kysymysMaara + " (" + Math.round( (aihepiiri_id.oikeinMaara/aihepiiri_id.kysymysMaara*100) ) + "%)"
          tulos2 += newline + newline
          return tulos2       
      })
    return tulos
  }

  // Ryhmitelty groupedData data-taulukko:
  let groupedData2 = props.data.reduce( reducer, [] );

  console.log(groupedData2)

  return (
    
    <div className="stats">
      <h2>Tulokset</h2>
      
      <h3>Tulokset aihepiireittäin</h3>
      <span id="naytaTulokset">{naytaTulokset(groupedData2).map( (tulos, index) => {
          return <p key={index}>{tulos}</p>
      })}</span>
  

      <div className="stats__graph">
        
      </div>
    </div>
    
  );
}

export default Stats;