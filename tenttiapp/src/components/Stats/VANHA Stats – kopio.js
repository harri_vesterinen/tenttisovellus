import React from 'react';

// käytä jotain google charttia piirtämiseen

import './Stats.css';

function Stats(props) {

  // reducer funktio:
  const reducer = (groupedData, currentItem) => {
    // haetaan index kysymyksen aihepiiri_id:n mukaan groupedData-taulukosta
    var index = groupedData.findIndex( item => item.aihepiiri_id === currentItem.aihepiiri_id );

    // käy läpi kysymykset
    props.data.map( kysymys => {
      // haetaan index kysymyksen aihepiirin mukaan groupedData-taulukosta
      index = groupedData.findIndex( kysymys => kysymys.aihepiiri_id === currentItem.aihepiiri_id );
      let KysymysOikein = true
      // käy läpi vastaukset
      kysymys.vastaukset.map(vastaus => {
        if (vastaus.kayttajan_vastaus !== vastaus.oikein) {
          KysymysOikein = false
        }
      return vastaus
      })

      // jos index löytyy eli kysymyksen aihepiiri on jo groupedData-taulukossa
      if ( index >= 0 ) {   
        if (KysymysOikein) {
          groupedData[index].oikeinMaara = groupedData[index].oikeinMaara + 1;
        } else {
          groupedData[index].vaarinMaara = groupedData[index].vaarinMaara + 1;
        }
        groupedData[index].kysymysMaara = groupedData[index].kysymysMaara + 1;

      } else {
        // aihepiiriä ei vielä ole -> lisätään groupedDatan loppuun uusi aihepiiri ja onko vastaus oikein:
        if (KysymysOikein) {
          groupedData.push( { aihepiiri_id: currentItem.aihepiiri_id, oikeinMaara: 1, vaarinMaara: 0, kysymysMaara: 1} );
        } else {
          groupedData.push( { aihepiiri_id: currentItem.aihepiiri_id, oikeinMaara: 0, vaarinMaara: 1, kysymysMaara: 1 } );
        }
      }
      return kysymys
    })

    // palautetaan groupedData-taulukko:
    return groupedData;
  }

  // tulokset
  function naytaTulokset(groupedData) {   
    console.log("Näytätulokset")
    var newline = String.fromCharCode(13, 10);
    var tulos = "";
    tulos = groupedData.map( (aihepiiri_id) => {  
        
          tulos += "Aihepiiri_id: " + aihepiiri_id.aihepiiri_id
          tulos += ", oikein kpl: " + aihepiiri_id.oikeinMaara + ", väärin kpl: " + aihepiiri_id.vaarinMaara + ", aihepiirin kysymyksiä yht: " + aihepiiri_id.kysymysMaara
          tulos += newline
          tulos += "------"
          return tulos    
        
      })
    return tulos
  }

  // Ryhmitelty groupedData data-taulukko:
  let groupedData = props.data.reduce( reducer, [] );

  console.log(groupedData)

  return (
    
    <div className="stats">
      <h2>Tilastot</h2>
      
      <h3>Tulokset aihepiireittäin</h3>
      <b>{naytaTulokset(groupedData)}</b>
  

      <div className="stats__graph">
        
      </div>
    </div>
    
  );
}

export default Stats;