const GetUserToken = () => {
    var userToken = null
    if (localStorage.getItem('TenttiAppUserToken')) {
        userToken = JSON.parse(localStorage.getItem('TenttiAppUserToken')).data.token
    }    
    return userToken
}

export default GetUserToken;