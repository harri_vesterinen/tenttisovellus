import React, { useContext } from 'react';
import { Button } from '@material-ui/core';
import {TenttiContext} from '../../Context'

const TenttiSort = (props) => {
    
    const { tentit, dispatch } = useContext(TenttiContext);

    // piirto:
    return(
        <div>
            {props.kayttaja !== null && props.aakkosjärjestys!==true && props.onkoAdmin===true &&
            // tenttien sorttaustoiminto: aakkosjärjestykseen
            <Button variant="outlined" color="primary"
                onClick={ () => { props.setAakkosjärjestys(true)
                                  props.setValittuTentti(null)
                                  dispatch({tapahtumatyyppi: 'examSort', järjestys: 'zToa' })
                                } }>Öökköstä tentit
            </Button>
            }

            {props.kayttaja !== null && props.aakkosjärjestys!==false && props.onkoAdmin===true &&
            // tenttien sorttaustoiminto: öökkösjärjestykseen
            <Button variant="outlined" color="primary"
            onClick={ () => { props.setAakkosjärjestys(false)
                              props.setValittuTentti(null)
                              dispatch({tapahtumatyyppi: 'examSort', järjestys: 'aToz' })                     
                            } }>Aakkosta tentit
            </Button>
            }
        </div>
    );

}

export default TenttiSort;