import React from 'react'; // ,{ useState, useEffect } poistettu
import useEffect from 'react';
import { Box, Checkbox, FormLabel } from '@material-ui/core'; // Button poistettu
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
//import FormControlLabel from '@material-ui/core/FormControlLabel';
import { green, red } from '@material-ui/core/colors'; // lightGreen poistettu
import Paper from '@material-ui/core/Paper';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
//import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
//import Grid from '@material-ui/core/Grid';
import DeleteForeverRoundedIcon from '@material-ui/icons/DeleteForeverRounded';
import AddCircleOutlineRoundedIcon from '@material-ui/icons/AddCircleOutlineRounded';
//import { flexbox } from '@material-ui/system';

import GetUserToken from '../Functions/GetUserToken'

const axios = require('axios');

export default function Admin(props) {

    //********* */
    const useStyles = makeStyles(theme => ({
        container: {
          display: 'flex',
          flexWrap: 'wrap',
          marginLeft: theme.spacing(2),
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 700,
        },
        formControl: {
            margin: theme.spacing(2),
            marginLeft: theme.spacing(1),
            minWidth: 120,
        },
        
      }));
    const classes = useStyles();
    /************ */

    // vihreä CheckBox
    const GreenCheckbox = withStyles({
        root: {
          color: green[400],
          '&$checked': {
            color: green[600],
          },
        },
        checked: {},
    })(props => <Checkbox {...props} />);

    // aihepiirin valintalistakentän vaihtoehtojen mäppäys
    function aihepiiriRows(props) {
        let aihepiiriRivit = props.aihePiirit.map((aihepiiri) => {
            let aihepiiriRivi = <MenuItem key={aihepiiri.id} value={aihepiiri.id}>{aihepiiri.nimi}</MenuItem>
            return (aihepiiriRivi)
        })
        return aihepiiriRivit
    }

    
    // piirto
    return (
    
        <Paper>
          
            <Box>
                { /*   ***** EI KÄYTÖSSÄ ******
                <TextField        
                    className={classes.textField}          
                    disabled 
                    style={{ marginLeft: 10, marginTop: 10, fontSize: 30 }}
                    id="lisaa_kysymys-standard-disabled" 
                    label={props.tenttiNimi}
                    defaultValue={"Lisää uusi kysymys numero " + (props.kysymysIndex+1)}
                    variant="standard"
                />
                ***** EI KÄYTÖSSÄ ****** */ }

                { /*   ***** EI KÄYTÖSSÄ ******
                <AddCircleOutlineRoundedIcon 
                    style={{ color: green[500], marginLeft: 10, marginTop: 20, fontSize: 30 }}
                    //VANHA: onClick={(event) => props.handleAddNewQuestionAdmin(event, props.tenttiIndex, props.kysymysIndex) }
                    onClick={() => {
                        // SQL-haku: POST / localhost:5000/kysymys/{tentti_id}
                        var userToken = GetUserToken()
                        axios.post(props.url + 'kysymys/' + props.tentti_id , {
                            // ei bodyssä mitään tartte viedä
                            } , { headers: {"x-access-token" : `${userToken}`} } )
                            .then(function (response) {
                                console.log(response);
                                console.log('kysymys_id: ' + response.data[0].id)
                                props.dispatch(
                                    {   nimi: "questionAddAdmin",
                                        arvo: undefined,
                                        tapahtumatyyppi: 'lisää',
                                        tenttiIndex: props.tenttiIndex,
                                        kysymysIndex: props.kysymysIndex,
                                        vastausIndex: undefined,
                                        kysymys_id: response.data[0].id
                                    }
                                )
                            })
                            .catch(function (error) {
                                console.error('Kysymyksen lisääminen epäonnistui (' + error + ')');
                                alert('Kysymyksen lisääminen epäonnistui. Kirjaudu sisään uudelleen sovellukseen, mikäli virhe toistuu.')
                                //handleKirjauduUlos()
                            })
                        }
                        // VANHA:
                        //props.dispatch(
                        //    {nimi: "questionAddAdmin",
                        //    arvo: undefined,
                        //    tapahtumatyyppi: 'lisää',
                        //    tenttiIndex: props.tenttiIndex,
                        //    kysymysIndex: props.kysymysIndex,
                        //    vastausIndex: undefined }
                        // )
                                                                                      
                    }
                /> 
                ***** EI KÄYTÖSSÄ ****** */ }

            </Box>

            <Box>
                <TextField
                required
                id={props.tenttiIndex + props.kysymysIndex } // oli: + props.teksti
                label={"Muokkaa kysymystä numero " + (props.kysymysIndex+1)}
                defaultValue={props.teksti}
                //value={props.teksti}
                className={classes.textField}
                helperText={props.tenttiNimi}
                margin="normal"
                variant="filled"
                // VANHA:
                //onChange={(event) => { document.getElementById(props.tenttiIndex + props.kysymysIndex + props.kysymys).value =                        
                //          props.handleQuestionChangeAdmin(event, props.tenttiIndex, props.kysymysIndex) }
                //      }
                onBlur={(event, currentValue) => { document.getElementById(props.tenttiIndex + props.kysymysIndex + props.key ).value =    // oli: + props.teksti                   
                    // TÄHÄN SQL-KYSELY: localhost:5000/kysymys/{kysymys_id}
                    currentValue = event.target.value
                    var userToken = GetUserToken()
                    axios.put(props.url + 'kysymys/' + props.kysymys_id, {
                        teksti: currentValue
                        } , { headers: {"x-access-token" : `${userToken}`} } )
                        .then(function (response) {
                            console.log(response);
                            props.dispatch(
                                {   nimi: "questionChangeAdmin",
                                    arvo: currentValue,
                                    tapahtumatyyppi: 'muuta',
                                    tenttiIndex: props.tenttiIndex,
                                    kysymysIndex: props.kysymysIndex,
                                    vastausIndex: undefined, 
                                    id: response.data[0].id
                                }
                            )
                            document.getElementById(props.tenttiIndex + props.kysymysIndex + props.key ).value = currentValue
                        })
                        .catch(function (error) {
                            console.error('Kysymyksen tekstin talletus epäonnistui (' + error + ')');
                            alert('Kysymyksen tekstin talletus epäonnistui. Kirjaudu sisään uudelleen.')
                            props.handleKirjauduUlos()
                        });

                    // VANHA:
                    /*
                    props.dispatch(
                        {   nimi: "questionChangeAdmin",
                            arvo: event.target.value,
                            tapahtumatyyppi: 'muuta',
                            tenttiIndex: props.tenttiIndex,
                            kysymysIndex: props.kysymysIndex,
                            vastausIndex: undefined 
                        }
                    )*/
                    }
                }
                />
            
                <FormControl variant="filled" className={classes.formControl}>
                    <InputLabel id={props.tenttiIndex + props.kysymysIndex + props.aihepiiri}>Aihepiiri</InputLabel>
                    <Select
                        labelId={props.tenttiIndex + props.kysymysIndex + props.aihepiiri_id.toString()} //oli: props.aihepiiri
                        id={props.tenttiIndex + props.kysymysIndex + props.aihepiiri_id.toString()}
                        value={props.aihepiiri_id}  // oli: props.aihepiiri                
                        //VANHA: onChange={(event) => { document.getElementById(props.tenttiIndex + props.kysymysIndex + props.aihepiiri).value =                        
                        //    props.handleQuestionCategoryChangeAdmin(event, props.tenttiIndex, props.kysymysIndex) }
                        //}
                        onChange={(event, currentValue) => { document.getElementById(props.tenttiIndex + props.kysymysIndex + props.aihepiiri_id.toString()).value = // oli: props.aihepiiri
                            // TÄHÄN SQL-KYSELY: localhost:5000/kysymys/{kysymys_id}
                            currentValue = event.target.value
                            var userToken = GetUserToken()
                            axios.put(props.url + 'kysymys/' + props.kysymys_id, {
                                aihepiiri_id: currentValue
                                } , { headers: {"x-access-token" : `${userToken}`} } )
                                .then(function (response) {
                                    console.log(response);
                                    props.dispatch(
                                        {   nimi: "questionCategoryChangeAdmin",
                                            arvo: currentValue,
                                            tapahtumatyyppi: 'muuta',
                                            tenttiIndex: props.tenttiIndex,
                                            kysymysIndex: props.kysymysIndex,
                                            vastausIndex: undefined
                                        }
                                    )
                                })
                                .catch(function (error) {
                                    console.error('Kysymyksen aihepiirin talletus epäonnistui (' + error + ')');
                                    alert('Kysymyksen aihepiirin talletus epäonnistui. Kirjaudu sisään uudelleen.')
                                    props.handleKirjauduUlos()
                                });                          
                            }
                            /* VANHA
                            props.dispatch(
                                {   nimi: "questionCategoryChangeAdmin",
                                    arvo: event.target.value,
                                    tapahtumatyyppi: 'muuta',
                                    tenttiIndex: props.tenttiIndex,
                                    kysymysIndex: props.kysymysIndex,
                                    vastausIndex: undefined 
                                }
                            ) }
                            */                          
                        }
                    >
                        {   // hae aihepiirit valintalistaan:
                        aihepiiriRows(props)
                        }                       
                    </Select>
                </FormControl>

                <DeleteForeverRoundedIcon 
                    style={{ color: red[500], marginTop: 25, fontSize: 30 }}
                    //VANHA: onClick={(event) => props.handleDeleteQuestionAdmin(event, props.tenttiIndex, props.kysymysIndex) }
                    onClick={() => {
                        // SQL-haku: DELETE / localhost:5000/kysymys/{kysymys_id}
                        var userToken = GetUserToken()
                        axios.delete(props.url + 'kysymys/' + props.kysymys_id , { headers: {"x-access-token" : `${userToken}`} } )
                            .then(function (response) {
                                console.log(response);
                                console.log('kysymys_id: ' + response.data[0].id)
                                props.dispatch(
                                    {   
                                        nimi: "deleteQuestionAdmin",
                                        arvo: undefined,
                                        tapahtumatyyppi: 'poista',
                                        tenttiIndex: props.tenttiIndex,
                                        kysymysIndex: props.kysymysIndex,
                                        vastausIndex: undefined
                                    }
                                )
                                // ei toimi document.getElementById(props.tenttiIndex + props.kysymysIndex + props.key ).value = response.data[0].teksti
                            })
                            .catch(function (error) {
                                console.error('Kysymyksen poistaminen epäonnistui (' + error + ')');
                                alert('Kysymyksen poistaminen epäonnistui. Poista ensin kysymyksen vastaukset. Ellei se auta, kirjaudu sisään uudelleen sovellukseen.')
                                //props.handleKirjauduUlos()
                            })
                        }

                        /* VANHA                      
                            props.dispatch(
                                {nimi: "deleteQuestionAdmin",
                                arvo: undefined,
                                tapahtumatyyppi: 'poista',
                                tenttiIndex: props.tenttiIndex,
                                kysymysIndex: props.kysymysIndex,
                                vastausIndex: undefined }
                            )              
                        */
                    }                 
                />
            
            </Box>

            {props.vastaukset.map( (vastaus, i) => (

                <Box key={i.toString()}>

                    <Box>
                        <AddCircleOutlineRoundedIcon 
                            style={{ color: green[500], marginLeft: 380, fontSize: 30 }}
                            //VANHA: onClick={(event) => props.handleAddNewAnswerAdmin(event, props.tenttiIndex, props.kysymysIndex, i) }
                            onClick={() => {
                                // SQL-haku: POST / localhost:5000/vastaus/{kysymys_id}
                                var userToken = GetUserToken()
                                axios.post(props.url + 'vastaus/' + props.kysymys_id , {
                                    // ei bodyssä mitään tartte viedä
                                    } , { headers: {"x-access-token" : `${userToken}`} } )
                                    .then(function (response) {
                                        console.log(response);
                                        console.log('vastaus_id: ' + response.data[0].id)
                                        props.dispatch(
                                            {   
                                                nimi: "answerAddAdmin",
                                                arvo: undefined,
                                                tapahtumatyyppi: 'lisää',
                                                tenttiIndex: props.tenttiIndex,
                                                kysymysIndex: props.kysymysIndex,
                                                vastausIndex: i,
                                                vastaus_id: response.data[0].id,
                                                kysymys_id: response.data[0].kysymys_id
                                            }
                                        )
                                    })
                                    .catch(function (error) {
                                        console.error('Vastauksen lisääminen epäonnistui (' + error + ')');
                                        alert('Vastauksen lisääminen epäonnistui. Kirjaudu sisään uudelleen sovellukseen, mikäli virhe toistuu.')
                                        //handleKirjauduUlos()
                                    })
                                }

                                /* VANHA:
                                props.dispatch(
                                    {id: vastaus.id, // HUOM responsesta
                                    nimi: "answerAddAdmin",
                                    arvo: undefined,
                                    tapahtumatyyppi: 'lisää',
                                    tenttiIndex: props.tenttiIndex,
                                    kysymysIndex: props.kysymysIndex,
                                    vastausIndex: i }
                                )
                                */
                            }
                        />
                    
                    </Box>

                    <FormLabel>
                        <GreenCheckbox
                            checked={vastaus.oikein} // oli vastaus.oikeavastaus
                            //VANHA: onChange= {(event) => props.handleCheckboxChangeAdmin(event, props.tenttiIndex, props.kysymysIndex, i) }
                            onChange={(event, currentValue) => {  //poistettu: document.getElementById(props.tenttiIndex + props.kysymysIndex + i).checked =     + poistettu: + vastaus.teksti).value
                                // TÄHÄN SQL-KYSELY: localhost:5000/vastaus/{vastaus_id}
                                currentValue = event.target.checked
                                var userToken = GetUserToken()
                                axios.put(props.url + 'vastaus/' + vastaus.id, { 
                                    oikein: currentValue
                                    } , { headers: {"x-access-token" : `${userToken}`} } )
                                    .then(function (response) {
                                        console.log(response);
                                        //console.log('vastaus_id: ' + response.data[0].id);
                                        props.dispatch(
                                            {   nimi: "checkboxChangeAdmin",
                                                arvo: currentValue,
                                                tapahtumatyyppi: 'muuta',
                                                tenttiIndex: props.tenttiIndex,
                                                kysymysIndex: props.kysymysIndex,
                                                vastausIndex: i,
                                                id: response.data[0].id
                                            }
                                        )
                                    })
                                    .catch(function (error) {
                                        console.error('Oikean vastauksen muuttaminen epäonnistui (' + error + ')');
                                        alert('Oikean vastauksen muuttaminen epäonnistui. Kirjaudu sisään uudelleen sovellukseen, mikäli virhe toistuu.')
                                        //handleKirjauduUlos()
                                    });
    
                                // VANHA:
                                /*
                                onChange={
                                (event) => props.dispatch(
                                    {nimi: "checkboxChangeAdmin",
                                    arvo: event.target.checked,
                                    tapahtumatyyppi: 'muuta',
                                    tenttiIndex: props.tenttiIndex,
                                    kysymysIndex: props.kysymysIndex,
                                    vastausIndex: i }
                                    )
                                }
                                */
                                }
                            }                          
                        >
                        </GreenCheckbox>
                    </FormLabel>                   

                    <TextField
                        required
                        id={props.tenttiIndex + props.kysymysIndex + props.key + i} // oli: + vastaus.teksti
                        label="Muokkaa vastausta. Jos vastaus on oikea vastaus, valitse vasemman puolen ruksi."
                        defaultValue={vastaus.teksti}
                        //value={vastaus.teksti}
                        className={classes.textField}
                        helperText=""
                        margin="normal"
                        variant="standard"
                        //VANHA: onChange={(event) => { document.getElementById(props.tenttiIndex + props.kysymysIndex + i + vastaus.vastaus).value =                        
                        //        props.handleAnswerChangeAdmin(event, props.tenttiIndex, props.kysymysIndex, i) }
                        //        }

                        onBlur={(event, currentValue) => { document.getElementById(props.tenttiIndex + props.kysymysIndex + props.key + i).value =    // oli vastaus.vastaus
                            // TÄHÄN SQL-KYSELY: localhost:5000/vastaus/{vastaus_id}
                            currentValue = event.target.value
                            var userToken = GetUserToken()
                            axios.put(props.url + 'vastaus/' + vastaus.id, { 
                                teksti: currentValue
                                } , { headers: {"x-access-token" : `${userToken}`} } )
                                .then(function (response) {
                                    console.log(response);
                                    //console.log('vastaus_id: ' + response.data[0].id);
                                    props.dispatch(
                                        {   nimi: "answerChangeAdmin",
                                            arvo: currentValue,
                                            tapahtumatyyppi: 'muuta',
                                            tenttiIndex: props.tenttiIndex,
                                            kysymysIndex: props.kysymysIndex,
                                            vastausIndex: i,
                                            id: response.data[0].id
                                        }
                                    )
                                    document.getElementById(props.tenttiIndex + props.kysymysIndex + props.key + i).value = currentValue
                                })
                                .catch(function (error) {
                                    console.error('Vastauksen tekstin talletus epäonnistui (' + error + ')');
                                    alert('Vastauksen tekstin talletus epäonnistui. Kirjaudu sisään uudelleen.')
                                    props.handleKirjauduUlos()
                                });

                            // VANHA:
                            /*
                            props.dispatch(
                                {   nimi: "answerChangeAdmin",
                                    arvo: event.target.value,
                                    tapahtumatyyppi: 'muuta',
                                    tenttiIndex: props.tenttiIndex,
                                    kysymysIndex: props.kysymysIndex,
                                    vastausIndex: i
                                }
                            )
                            */
                            }
                        }
                    />
                                          
                    <DeleteForeverRoundedIcon 
                        style={{ color: red[500], marginTop: 15, fontSize: 30 }}
                        //VANHA: onClick={(event) => props.handleDeleteAnswerAdmin(event, props.tenttiIndex, props.kysymysIndex, i) }
                        onClick={() => {
                            // SQL-haku: DELETE / localhost:5000/vastaus/{vastaus_id}
                            var userToken = GetUserToken()
                            axios.delete(props.url + 'vastaus/' + vastaus.id , { headers: {"x-access-token" : `${userToken}`} } )
                                .then(function (response) {
                                    console.log(response);
                                    console.log('vastaus_id: ' + response.data[0].id)
                                    props.dispatch(
                                        {   
                                            nimi: "deleteAnswerAdmin",
                                            arvo: undefined,
                                            tapahtumatyyppi: 'poista',
                                            tenttiIndex: props.tenttiIndex,
                                            kysymysIndex: props.kysymysIndex,
                                            vastausIndex: i
                                        }
                                    )
                                })
                                .catch(function (error) {
                                    console.error('Vastauksen poistaminen epäonnistui (' + error + ')');
                                    alert('Vastauksen poistaminen epäonnistui. Vastauksen poistoa ei voi tehdä, koska opiskelijoilla on siihen liittyviä tenttivastauksia.')
                                    //handleKirjauduUlos()
                                })
                            }
    
                            /* VANHA
                            onClick={() => 
                                props.dispatch(
                                    {nimi: "deleteAnswerAdmin",
                                    arvo: undefined,
                                    tapahtumatyyppi: 'poista',
                                    tenttiIndex: props.tenttiIndex,
                                    kysymysIndex: props.kysymysIndex,
                                    vastausIndex: i }
                                )
                            }
                            */
                        }
                    />
                 
                </Box>
            )) }

            <Box>
                <AddCircleOutlineRoundedIcon 
                    style={{ color: green[500], marginLeft: 380, fontSize: 30 }}
                    //VANHA: onClick={(event) => props.handleAddNewAnswerAdmin(event, props.tenttiIndex, props.kysymysIndex) }

                    onClick={() => {
                        // SQL-haku: POST / localhost:5000/vastaus/{kysymys_id}
                        var userToken = GetUserToken()
                        axios.post(props.url + 'vastaus/' + props.kysymys_id , {
                            // ei bodyssä mitään tartte viedä
                            } , { headers: {"x-access-token" : `${userToken}`} } )
                            .then(function (response) {
                                console.log(response);
                                console.log('vastaus_id: ' + response.data[0].id)
                                props.dispatch(
                                    {   
                                        nimi: "answerAddAdmin",
                                        arvo: undefined,
                                        tapahtumatyyppi: 'lisää',
                                        tenttiIndex: props.tenttiIndex,
                                        kysymysIndex: props.kysymysIndex,
                                        //vastausIndex: i,
                                        vastausIndex: undefined,
                                        vastaus_id: response.data[0].id,
                                        kysymys_id: response.data[0].kysymys_id
                                    }
                                )
                            })
                            .catch(function (error) {
                                console.error('Vastauksen lisääminen epäonnistui (' + error + ')');
                                alert('Vastauksen lisääminen epäonnistui. Kirjaudu sisään uudelleen sovellukseen, mikäli virhe toistuu.')
                                //handleKirjauduUlos()
                            })
                        }

                        /* VANHA
                        onClick={(event) => 
                            props.dispatch(
                                {nimi: "answerAddAdmin",
                                arvo: undefined,
                                tapahtumatyyppi: 'lisää',
                                tenttiIndex: props.tenttiIndex,
                                kysymysIndex: props.kysymysIndex,
                                vastausIndex: undefined }
                            )
                        }
                        */
                    }                   
                />
            </Box>

        </Paper>
    );
}