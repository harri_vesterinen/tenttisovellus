import React from 'react';
//import Button from '@material-ui/core/Button';
import { Box, Button } from '@material-ui/core';
//import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

//import DeleteForeverRoundedIcon from '@material-ui/icons/DeleteForeverRounded';
import InfoSharpIcon from '@material-ui/icons/InfoSharp';
import { green } from '@material-ui/core/colors';

// *******  SOVELLUKSEN INFO-DIALOGI *********
export default function DialogAlertInfoOfApp(props) {

  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  // piirto
  return (
    <Box>
      
        <InfoSharpIcon 
                        style={{ color: green[300], marginTop: 5, fontSize: 30 }}
                        onClick={() => {handleClickOpen()} }  
        />
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{"Tietoa tenttisovelluksesta"}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    Toteutus Harri Vesterinen 2020:<br />
                    - sovellus on toteutettu Fullstack -kehittäjän kurssilla Tampereella ajalla 10/2019-02/2020.<br />
                    - frontend-toteutus: React/Javascript<br />
                    - backend-toteutus: node.js/express.js -sovelluspalvelin, PostgreSQL -relaatiotietokanta.<br />
                    <br />
                    Sovelluksen käyttäjäroolit:<br />
                    - opiskelija (peruskäyttäjä) voi rekisteröityä/kirjautua sovellukseen.<br />
                    - kirjautunut opiskelija voi suorittaa tentittävissä olevia tenttejä. <br />
                    - tenttien ylläpitäjä (admin-käyttäjä) voi lisätä/muokata/poistaa tenttejä, kysymyksiä ja vastauksia.<br />
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary" autoFocus>
                    OK
                </Button>
            </DialogActions>
        </Dialog>

    </Box>
  );
}