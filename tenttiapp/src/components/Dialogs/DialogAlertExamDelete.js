import React from 'react';
//import Button from '@material-ui/core/Button';
import { Box, Button } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import DeleteForeverRoundedIcon from '@material-ui/icons/DeleteForeverRounded';
import { red } from '@material-ui/core/colors';

import GetUserToken from '../Functions/GetUserToken'

const axios = require('axios');

// *******  TENTIN POISTODIALOGI *********
export default function DialogAlertExamDelete(props) {

  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  // piirto
  return (
    <Box>
      
        <DeleteForeverRoundedIcon 
                        style={{ color: red[500], marginTop: 10, fontSize: 30 }}
                        onClick={(event) => {handleClickOpen()} }  
        />
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{"Poista tentti?"}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    Haluatko varmasti poistaa tämän tentin?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary" autoFocus>
                    Peruuta
                </Button>
                <Button 
                    //VANHA: onClick={ (event) => props.handleDeleteExamAdmin(event, props.tenttiIndex)} color="primary"
                    onClick={() => {
                        // SQL-haku: DELETE / localhost:5000/tentti/{tentti_id}
                        var userToken = GetUserToken()
                        axios.delete(props.url + 'tentti/' + props.tentti_id , { headers: {"x-access-token" : `${userToken}`} })
                            .then(function (response) {
                                console.log(response);
                                console.log('poista tentti_id: ' + response.data[0].id)
                                props.setValittuTentti(null)
                                props.dispatch(
                                    {   
                                        nimi: "examDeleteAdmin",
                                        arvo: undefined,
                                        tapahtumatyyppi: 'poista',
                                        tenttiIndex: props.tenttiIndex,
                                        kysymysIndex: undefined,
                                        vastausIndex: undefined
                                    }
                                )
                            })
                            .catch(function (error) {
                                console.log(error + ':' + error.response.data);
                                alert(error.response.data)
                                handleClose() // kutsu handleClose(), muuten dialogi aukeaa uudelleen!
                            })
                        }

                        /* VANHA                      
                            {props.dispatch(
                                {nimi: "examDeleteAdmin",
                                arvo: undefined,
                                tapahtumatyyppi: 'poista',
                                tenttiIndex: props.tenttiIndex,
                                kysymysIndex: undefined,
                                vastausIndex: undefined }
                            )
                            props.setValittuTentti(null)
                            }            
                        */
                    }  
                    color="primary"                  
                >
                    Poista
                </Button>
            </DialogActions>
        </Dialog>
        
        <TextField        
            //className={classes.textField}          
            disabled 
            style={{ marginLeft: 10, marginTop: 10, marginBottom: 30, fontSize: 30 }}
            id="poista_tentti-standard-disabled" 
            //label={tentit[valittuTentti].nimi}
            defaultValue={"Poista tentti"}
            variant="standard"
        /> 

    </Box>
  );
}