import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
//import Button from '@material-ui/core/Button';
import { Box, Button } from '@material-ui/core';
//import IconButton from '@material-ui/core/IconButton';
//import MenuIcon from '@material-ui/icons/Menu';

import DialogAlertInfoOfApp from '../Dialogs/DialogAlertInfoOfApp'
import GetUserToken from '../Functions/GetUserToken'

const axios = require('axios');

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

// *********************************************

// {props.kayttaja !== null && <Button color="inherit" onClick={props.handleKirjauduUlos(props.kayttaja)}>Poistu</Button>}

export default function ButtonAppBar(props) {
  const classes = useStyles();

  // piirto
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          
          <Typography variant="h6" className={classes.title}>
            Tenttisovellus
          </Typography>
          <Typography variant="body1" className={classes.title}>
            {props.kayttajaNimi !== null && props.onkoAdmin === true && (props.kayttajaNimi + ", admin-käyttäjä") }
            {props.kayttajaNimi !== null && props.onkoAdmin === false && (props.kayttajaNimi + ", opiskelija") }
            {props.valittuTentti !== null && ", " + props.tentit[props.valittuTentti].nimi }
          </Typography>
          {props.kayttaja !== null && <Button color="inherit"
                                        onClick={ () => {
                                          props.handleNaytaTentit()
                                          props.setOnkoTentitHaettu(true)
                                        }}
                                      >Tentit</Button>}
          {props.onkoAdmin === true && props.tenttiLength>0 && <Button color="inherit" 
                                          //VANHA: onClick={ () => props.handleAddNewExamAdmin() }
                                          onClick={() => {
                  
                                            // SQL-haku: POST / localhost:5000/tentti
                                            var userToken = GetUserToken()
                                            axios.post(props.url + 'tentti' , {
                                                // ei bodyssä mitään tartte viedä
                                                } , { headers: {"x-access-token" : `${userToken}`} } )
                                                .then(function (response) {
                                                    console.log(response);
                                                    console.log('tentti_id: ' + response.data[0].id)
                                                    props.dispatch(
                                                        {   nimi: "examAddAdmin",
                                                            arvo: undefined,
                                                            tapahtumatyyppi: 'lisää',
                                                            tenttiIndex: undefined,
                                                            kysymysIndex: undefined,
                                                            vastausIndex: undefined,
                                                            tentti_id: response.data[0].id
                                                        }
                                                    )
                                                    props.setValittuTentti(props.tenttiLength)
                                                })
                                                .catch(function (error) {
                                                    alert('Virhe tentin lisäämisessä (Virhe: ' + error + ')')
                                                    console.log(error);
                                                })
                                              }
                                            /* VANHA:
                                            onClick={() => 
                                              {props.dispatch(
                                                  {nimi: "examAddAdmin",
                                                  arvo: undefined,
                                                  tapahtumatyyppi: 'lisää',
                                                  tenttiIndex: undefined,
                                                  kysymysIndex: undefined,
                                                  vastausIndex: undefined }
                                              )
                                              props.setValittuTentti(props.tenttiLength)
                                              }
                                            }
                                            */                                                                
                                          }                                
                                        >Lisää tentti</Button>}
          { /* props.kayttaja === null && <Button color="inherit">Kirjaudu</Button> */ }
          {props.kayttaja !== null && <Button color="inherit" onClick={ () => props.handleKirjauduUlos( props.kayttaja ) }
                                      >Kirjaudu ulos</Button>}
          <Box>                            
                <DialogAlertInfoOfApp />              
          </Box>
        </Toolbar>
      </AppBar>
    </div>
  );
}
