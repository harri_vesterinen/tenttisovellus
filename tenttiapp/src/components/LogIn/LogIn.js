import React from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    marginLeft: theme.spacing(40),
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 500,
  },
}));

// **********************************
export default function LogIn(props) {
  const classes = useStyles();

  return (
    <form className={classes.container} noValidate autoComplete="off">
      <div>
        
        <div>
            <TextField
            disabled
            id="kirjaudu_otsikko"
            label=""
            defaultValue="KIRJAUDU tai REKISTERÖIDY"
            className={classes.textField}
            margin="normal"
            />
        </div>
       
        <div>
            <TextField
            required
            id="username"
            label="username"
            defaultValue=""
            className={classes.textField}
            helperText="Syötä käyttäjätunnuksesi"
            margin="normal"
            variant="filled"
            />
        </div>

        <div>
            <TextField
            id="password"
            label="password"
            className={classes.textField}
            type="password"
            autoComplete="current-password"
            helperText="Syötä salasanasi"
            margin="normal"
            variant="filled"
            />
        </div>

        <div>
            { /*
            <Button type="submit" variant="contained" color="primary" 
              onClick={ () => props.handleKirjauduSisaan( 
                                document.getElementById('username').value,
                                document.getElementById('password').value, 
                                false 
                              ) }>Kirjaudu opiskelija (vanha)</Button>
              */ }
        </div>

        <div>
            { /*
            <Button type="submit" variant="contained" color="primary" 
              onClick={ () => props.handleKirjauduSisaan(
                                document.getElementById('username').value,
                                document.getElementById('password').value,
                                true
                              ) }>Kirjaudu Admininä (vanha)</Button>
              */ }
        </div>

        <div>
            {
            <Button type="button" variant="contained" color="primary" // oli: type="submit"
              onClick={ () => props.handleKirjauduSisaanUUSI( 
                                document.getElementById('username').value,
                                document.getElementById('password').value
                              ) }>Kirjaudu</Button>
            }
        </div>

        <div>
            {
            <Button type="button" variant="contained" color="primary" // oli: type="submit"
              onClick={ () => props.handleOpiskelijaRekisteroidy(
                                document.getElementById('username').value,
                                document.getElementById('password').value,
                                false
                              ) }>Rekisteröidy</Button>
            }
        </div>

      </div>
    </form>
  );
}