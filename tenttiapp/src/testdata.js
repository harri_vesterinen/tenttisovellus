/* TENTTIRAKENNE:

*/

const testdata = 
[
    {   nimi: "Luonnontieteen koe",
        kysymykset: 
            [  {kysymys: "Mitä biologia tutkii?",
                aihepiiri: "Biologia",
                vastaukset:
                    [
                        {vastaus: "Elollisen luonnon ilmiöitä ja lainalaisuuksia maapallolla", valittu: false, oikeavastaus: true},
                        {vastaus: "Maapallon ja ulkoavaruuden ilmiöitä", valittu: false, oikeavastaus: false},
                        {vastaus: "Ulkoavaruuden mahdollisesti olevan elämän biokemiallista perustaa", valittu: false, oikeavastaus: false},
                        {vastaus: "Basic I/O -logiikkaa", valittu: false, oikeavastaus: false}
                    ]
                },

                {kysymys: "Kuka henkilö liittyy atomin keksimiseen?",
                aihepiiri: "Fysiikka",
                vastaukset:
                    [
                        {vastaus: "Aristoteles", valittu: false, oikeavastaus: false},
                        {vastaus: "Demokritos", valittu: false, oikeavastaus: true},
                        {vastaus: "Lappeenrannan torimyyjä", valittu: false, oikeavastaus: false},
                        {vastaus: "Demokriitikot", valittu: false, oikeavastaus: false}
                    ]
                },

                {kysymys: "Mistä käytetään lyhennettä Fe?",
                aihepiiri: "Fysiikka",
                vastaukset:
                    [
                        {vastaus: "Fenkoli", valittu: false, oikeavastaus: false},
                        {vastaus: "Rauta", valittu: false, oikeavastaus: true},
                        {vastaus: "Fetajuusto", valittu: false, oikeavastaus: false}
                    ]
                }                       
            ]
    },

    {   nimi: "Kulttuurin koe",
        kysymykset: 
            [  {kysymys: "Mitä kreikankielinen sana 'kissa' tarkoittaa suomeksi?",
                aihepiiri: "Kielet",
                vastaukset:
                    [
                        {vastaus: "närhi", valittu: false, oikeavastaus: true},
                        {vastaus: "varis", valittu: false, oikeavastaus: false},
                        {vastaus: "koira", valittu: false, oikeavastaus: false},
                        {vastaus: "kissa", valittu: false, oikeavastaus: false},
                        {vastaus: "kisa", valittu: false, oikeavastaus: false},
                        {vastaus: "harakka", valittu: false, oikeavastaus: true}
                    ]
                },

                {kysymys: "Mitkä seuraavista ovat keinotekoisia kieliä?",
                aihepiiri: "Kielet",
                vastaukset:
                    [
                        {vastaus: "swahili", valittu: false, oikeavastaus: false},
                        {vastaus: "hindi", valittu: false, oikeavastaus: false},
                        {vastaus: "esperanto", valittu: false, oikeavastaus: true},
                        {vastaus: "kosmos", valittu: false, oikeavastaus: true},
                        {vastaus: "Keski-Maan kielet", valittu: false, oikeavastaus: true},
                        {vastaus: "koltansaame", valittu: false, oikeavastaus: false}
                    ]
                },

                {kysymys: "Kuka on englantilaisen dekkarikirjailijan Peter Jamesin Brightoniin sijoittuvan kirjasarjan päähenkilö?",
                aihepiiri: "Kirjallisuus", 
                vastaukset:
                    [
                        {vastaus: "James Crichton", valittu: false, oikeavastaus: false},
                        {vastaus: "Roy Grace", valittu: false, oikeavastaus: true},
                        {vastaus: "Ann Hamilton", valittu: false, oikeavastaus: false},
                        {vastaus: "Ville Viksu", valittu: false, oikeavastaus: false}
                    ]
                },

                {kysymys: "Kuka/ketkä ovat kuvitteellisia ensimmäisen tai toisen maaimansodan hahmoja?",
                aihepiiri: "Kirjallisuus", 
                vastaukset:
                    [
                        {vastaus: "Hasse Wind", valittu: false, oikeavastaus: false},
                        {vastaus: "Battler Britton", valittu: false, oikeavastaus: true},
                        {vastaus: "Punainen parooni", valittu: false, oikeavastaus: false},
                        {vastaus: "Tex Willer", valittu: false, oikeavastaus: false},
                        {vastaus: "Marokon kauhu", valittu: false, oikeavastaus: false}
                    ]
                },

                {kysymys: "Mikä/mitkä seuraavista ovat suomalaisen bändin Amorphiksen levyjen nimiä?",
                aihepiiri: "Musiikki", 
                vastaukset:
                    [
                        {vastaus: "Under the Black Cloud", valittu: false, oikeavastaus: false},
                        {vastaus: "Skymaker", valittu: false, oikeavastaus: false},
                        {vastaus: "Silent Waters", valittu: false, oikeavastaus: true},
                        {vastaus: "Triangle", valittu: false, oikeavastaus: false},
                        {vastaus: "Queen of Time", valittu: false, oikeavastaus: true},
                        {vastaus: "Räkä poskelle ja jalat kattoon", valittu: false, oikeavastaus: false},
                        {vastaus: "Am Universum", valittu: false, oikeavastaus: true}
                    ]
                },

                {kysymys: "Mikä/mitkä ovat Matin ja Tepon biisejä??",
                aihepiiri: "Musiikki", 
                vastaukset:
                    [
                        {vastaus: "Puhelinlangat laulaa", valittu: false, oikeavastaus: false},
                        {vastaus: "Katson auringon siltaa", valittu: false, oikeavastaus: false},
                        {vastaus: "Mä joka päivä opiskelen", valittu: false, oikeavastaus: false},
                        {vastaus: "Kaapin takana on nainen", valittu: false, oikeavastaus: false},
                        {vastaus: "Mä joka päivä töitä teen", valittu: false, oikeavastaus: true}
                    ]
                },

                {kysymys: "Mihin elokuviin Hans Zimmer on säveltänyt musiikin?",
                aihepiiri: "Elokuvat", 
                vastaukset:
                    [
                        {vastaus: "The Lion King", valittu: false, oikeavastaus: true},
                        {vastaus: "Uuno Turhapuro menettää muistinsa", valittu: false, oikeavastaus: false},
                        {vastaus: "Inception", valittu: false, oikeavastaus: true},
                        {vastaus: "The Shining", valittu: false, oikeavastaus: false},
                        {vastaus: "Interstellar", valittu: false, oikeavastaus: true},
                        {vastaus: "Terminator I", valittu: false, oikeavastaus: false},
                        {vastaus: "Black Hawk Down", valittu: false, oikeavastaus: true}
                    ]
                }

            ]
    }

]

export default testdata;