import React from 'react'
import { TenttiProvider } from './Context'
import TenttiApp from './TenttiApp'

// ******  APP  **********
const App = () => {

    // TenttiApp-piirto
    return (

        <div className="App">
            <TenttiProvider>
                <TenttiApp />
            </TenttiProvider>
        </div>

    );

}

export default App;